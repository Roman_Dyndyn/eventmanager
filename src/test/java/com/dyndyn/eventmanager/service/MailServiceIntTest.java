package com.dyndyn.eventmanager.service;

import com.dyndyn.eventmanager.EventManagerApp;
import com.dyndyn.eventmanager.config.Constants;
import com.dyndyn.eventmanager.domain.User;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import io.github.jhipster.config.JHipsterProperties;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.MessageSource;
import org.springframework.test.context.junit4.SpringRunner;
import org.thymeleaf.spring5.SpringTemplateEngine;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = EventManagerApp.class)
@Ignore
public class MailServiceIntTest {

    @Autowired
    private JHipsterProperties jHipsterProperties;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private SpringTemplateEngine templateEngine;

    @Mock
    private SendGrid sendGrid;

    @Captor
    private ArgumentCaptor<Request> messageCaptor;

    private MailService mailService;

    @Before
    public void setup() throws IOException {
        MockitoAnnotations.initMocks(this);
        doReturn(mock(Response.class)).when(sendGrid).api(any(Request.class));
        mailService = new MailService(jHipsterProperties, sendGrid, messageSource, templateEngine);
    }

    @Test
    public void testSendEmail() throws Exception {
        mailService.sendEmail("john.doe@example.com", "testSubject", "testContent", false);
        verify(sendGrid).api(messageCaptor.capture());
        Request request = messageCaptor.getValue();
        assertThat(request.getEndpoint()).isEqualTo("mail/send");
        assertThat(request.getBody()).isEqualToIgnoringWhitespace("{\n" +
            "  \"from\": {\n" +
            "    \"email\": \"test@localhost\"\n" +
            "  },\n" +
            "  \"subject\": \"testSubject\",\n" +
            "  \"personalizations\": [\n" +
            "    {\n" +
            "      \"to\": [\n" +
            "        {\n" +
            "          \"email\": \"john.doe@example.com\"\n" +
            "        }\n" +
            "      ]\n" +
            "    }\n" +
            "  ],\n" +
            "  \"content\": [\n" +
            "    {\n" +
            "      \"type\": \"text/plain\",\n" +
            "      \"value\": \"testContent\"\n" +
            "    }\n" +
            "  ]\n" +
            "}");
    }

    @Test
    public void testSendHtmlEmail() throws Exception {
        mailService.sendEmail("john.doe@example.com", "testSubject", "testContent", true);
        verify(sendGrid).api(messageCaptor.capture());
        Request request = messageCaptor.getValue();
        assertThat(request.getEndpoint()).isEqualTo("mail/send");
        assertThat(request.getBody()).isEqualToIgnoringWhitespace("{\n" +
            "  \"from\": {\n" +
            "    \"email\": \"test@localhost\"\n" +
            "  },\n" +
            "  \"subject\": \"testSubject\",\n" +
            "  \"personalizations\": [\n" +
            "    {\n" +
            "      \"to\": [\n" +
            "        {\n" +
            "          \"email\": \"john.doe@example.com\"\n" +
            "        }\n" +
            "      ]\n" +
            "    }\n" +
            "  ],\n" +
            "  \"content\": [\n" +
            "    {\n" +
            "      \"type\": \"text/html\",\n" +
            "      \"value\": \"testContent\"\n" +
            "    }\n" +
            "  ]\n" +
            "}");
    }

    @Test
    public void testSendEmailFromTemplate() throws Exception {
        User user = new User();
        user.setLogin("john");
        user.setEmail("john.doe@example.com");
        user.setLangKey("en");
        mailService.sendEmailFromTemplate(user, "mail/testEmail", "email.test.title");
        verify(sendGrid).api(messageCaptor.capture());
        Request request = messageCaptor.getValue();
        assertThat(request.getEndpoint()).isEqualTo("mail/send");
        assertThat(request.getBody()).isEqualToIgnoringWhitespace("{\n" +
            "  \"from\": {\n" +
            "    \"email\": \"test@localhost\"\n" +
            "  },\n" +
            "  \"subject\": \"test title\",\n" +
            "  \"personalizations\": [\n" +
            "    {\n" +
            "      \"to\": [\n" +
            "        {\n" +
            "          \"email\": \"john.doe@example.com\"\n" +
            "        }\n" +
            "      ]\n" +
            "    }\n" +
            "  ],\n" +
            "  \"content\": [\n" +
            "    {\n" +
            "      \"type\": \"text/html\",\n" +
            "      \"value\": \"<html>test title, http://127.0.0.1:8080, john</html>\\n\"\n" +
            "    }\n" +
            "  ]\n" +
            "}");
    }

    @Test
    public void testSendActivationEmail() throws Exception {
        User user = new User();
        user.setLangKey(Constants.DEFAULT_LANGUAGE);
        user.setLogin("john");
        user.setEmail("john.doe@example.com");
        mailService.sendActivationEmail(user);
        verify(sendGrid).api(messageCaptor.capture());
        Request request = messageCaptor.getValue();
        assertThat(request.getEndpoint()).isEqualTo("mail/send");
        assertThat(request.getBody()).isEqualToIgnoringWhitespace("{\n" +
            "  \"from\": {\n" +
            "    \"email\": \"test@localhost\"\n" +
            "  },\n" +
            "  \"subject\": \"EventManager активація\",\n" +
            "  \"personalizations\": [\n" +
            "    {\n" +
            "      \"to\": [\n" +
            "        {\n" +
            "          \"email\": \"john.doe@example.com\"\n" +
            "        }\n" +
            "      ]\n" +
            "    }\n" +
            "  ],\n" +
            "  \"content\": [\n" +
            "    {\n" +
            "      \"type\": \"text/html\",\n" +
            "      \"value\": \"<!DOCTYPE html>\\n<html>\\n    <head>\\n        <title>EventManager активація</title>\\n        <meta http-equiv=\\\"Content-Type\\\" content=\\\"text/html; charset=UTF-8\\\" />\\n        <link rel=\\\"shortcut icon\\\" href=\\\"http://127.0.0.1:8080/favicon.ico\\\" />\\n    </head>\\n    <body>\\n        <p>Шановний john</p>\\n        <p>Ваш EventManager обліковий запис був створений. Будь ласка, натисніть на посилання нижче для його активації:</p>\\n        <p>\\n            <a href=\\\"http://127.0.0.1:8080/#/activate?key=null\\\">http://127.0.0.1:8080/#/activate?key=null</a>\\n        </p>\\n        <p>\\n            <span>З повагою,</span>\\n            <br/>\\n            <em>EventManager.</em>\\n        </p>\\n    </body>\\n</html>\\n\"\n" +
            "    }\n" +
            "  ]\n" +
            "}");
    }

    @Test
    public void testCreationEmail() throws Exception {
        User user = new User();
        user.setLangKey(Constants.DEFAULT_LANGUAGE);
        user.setLogin("john");
        user.setEmail("john.doe@example.com");
        mailService.sendCreationEmail(user);
        verify(sendGrid).api(messageCaptor.capture());
        Request request = messageCaptor.getValue();
        assertThat(request.getEndpoint()).isEqualTo("mail/send");
        assertThat(request.getBody()).isEqualToIgnoringWhitespace("{\n" +
            "  \"from\": {\n" +
            "    \"email\": \"test@localhost\"\n" +
            "  },\n" +
            "  \"subject\": \"EventManager активація\",\n" +
            "  \"personalizations\": [\n" +
            "    {\n" +
            "      \"to\": [\n" +
            "        {\n" +
            "          \"email\": \"john.doe@example.com\"\n" +
            "        }\n" +
            "      ]\n" +
            "    }\n" +
            "  ],\n" +
            "  \"content\": [\n" +
            "    {\n" +
            "      \"type\": \"text/html\",\n" +
            "      \"value\": \"<!DOCTYPE html>\\n<html>\\n    <head>\\n        <title>EventManager активація</title>\\n        <meta http-equiv=\\\"Content-Type\\\" content=\\\"text/html; charset=UTF-8\\\" />\\n        <link rel=\\\"shortcut icon\\\" href=\\\"http://127.0.0.1:8080/favicon.ico\\\" />\\n    </head>\\n    <body>\\n        <p>Шановний john</p>\\n        <p>Ваш EventManager обліковий запис був створений. Будь ласка, натисніть на посилання нижче для його активації:</p>\\n        <p>\\n            <a href=\\\"http://127.0.0.1:8080/#/reset/finish?key=null\\\">http://127.0.0.1:8080/#/reset/finish?key=null</a>\\n        </p>\\n        <p>\\n            <span>З повагою,</span>\\n            <br/>\\n            <em>EventManager.</em>\\n        </p>\\n    </body>\\n</html>\\n\"\n" +
            "    }\n" +
            "  ]\n" +
            "}");
    }

    @Test
    public void testSendPasswordResetMail() throws Exception {
        User user = new User();
        user.setLangKey(Constants.DEFAULT_LANGUAGE);
        user.setLogin("john");
        user.setEmail("john.doe@example.com");
        mailService.sendPasswordResetMail(user);
        verify(sendGrid).api(messageCaptor.capture());
        Request request = messageCaptor.getValue();
        assertThat(request.getEndpoint()).isEqualTo("mail/send");
        assertThat(request.getBody()).isEqualToIgnoringWhitespace("{\n" +
            "  \"from\": {\n" +
            "    \"email\": \"test@localhost\"\n" +
            "  },\n" +
            "  \"subject\": \"EventManager Скидання паролю\",\n" +
            "  \"personalizations\": [\n" +
            "    {\n" +
            "      \"to\": [\n" +
            "        {\n" +
            "          \"email\": \"john.doe@example.com\"\n" +
            "        }\n" +
            "      ]\n" +
            "    }\n" +
            "  ],\n" +
            "  \"content\": [\n" +
            "    {\n" +
            "      \"type\": \"text/html\",\n" +
            "      \"value\": \"<!DOCTYPE html>\\n<html>\\n    <head>\\n        <title>EventManager Скидання паролю</title>\\n        <meta http-equiv=\\\"Content-Type\\\" content=\\\"text/html; charset=UTF-8\\\" />\\n        <link rel=\\\"shortcut icon\\\" href=\\\"http://127.0.0.1:8080/favicon.ico\\\" />\\n    </head>\\n    <body>\\n        <p>Шановний john</p>\\n        <p>Для вашого EventManager облікового запису був створений запит на скидання пароля. Будь ласка, натисніть на посилання нижче для його активації:</p>\\n        <p>\\n            <a href=\\\"http://127.0.0.1:8080/#/reset/finish?key=null\\\">http://127.0.0.1:8080/#/reset/finish?key=null</a>\\n        </p>\\n        <p>\\n            <span>З повагою,</span>\\n            <br/>\\n            <em>EventManager.</em>\\n        </p>\\n    </body>\\n</html>\\n\"\n" +
            "    }\n" +
            "  ]\n" +
            "}");
    }

    @Test
    public void testSendEmailWithException() throws Exception {
        doThrow(IOException.class).when(sendGrid).api(any(Request.class));
        mailService.sendEmail("john.doe@example.com", "testSubject", "testContent", false);
    }

}
