package com.dyndyn.eventmanager.web.rest;

import com.dyndyn.eventmanager.EventManagerApp;

import com.dyndyn.eventmanager.domain.EventStudent;
import com.dyndyn.eventmanager.domain.Student;
import com.dyndyn.eventmanager.domain.Event;
import com.dyndyn.eventmanager.repository.EventStudentRepository;
import com.dyndyn.eventmanager.service.EventStudentService;
import com.dyndyn.eventmanager.service.dto.EventStudentDTO;
import com.dyndyn.eventmanager.service.mapper.EventStudentMapper;
import com.dyndyn.eventmanager.web.rest.errors.ExceptionTranslator;
import com.dyndyn.eventmanager.service.dto.EventStudentCriteria;
import com.dyndyn.eventmanager.service.EventStudentQueryService;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static com.dyndyn.eventmanager.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the EventStudentResource REST controller.
 *
 * @see EventStudentResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = EventManagerApp.class)
@ActiveProfiles("test")
@Ignore
public class EventStudentResourceIntTest {

    private static final Integer DEFAULT_MINUTES = 1;
    private static final Integer UPDATED_MINUTES = 2;

    private static final String DEFAULT_GOOGLE_EVENT_ID = "AAAAAAAAAA";
    private static final String UPDATED_GOOGLE_EVENT_ID = "BBBBBBBBBB";

    @Autowired
    private EventStudentRepository eventStudentRepository;

    @Autowired
    private EventStudentMapper eventStudentMapper;
    
    @Autowired
    private EventStudentService eventStudentService;

    @Autowired
    private EventStudentQueryService eventStudentQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restEventStudentMockMvc;

    private EventStudent eventStudent;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final EventStudentResource eventStudentResource = new EventStudentResource(eventStudentService, eventStudentQueryService);
        this.restEventStudentMockMvc = MockMvcBuilders.standaloneSetup(eventStudentResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EventStudent createEntity(EntityManager em) {
        EventStudent eventStudent = new EventStudent()
            .minutes(DEFAULT_MINUTES)
            .googleEventId(DEFAULT_GOOGLE_EVENT_ID);
        // Add required entity
        Student student = StudentResourceIntTest.createEntity(em);
        em.persist(student);
        em.flush();
        eventStudent.setStudent(student);
        // Add required entity
        Event event = EventResourceIntTest.createEntity(em);
        em.persist(event);
        em.flush();
        eventStudent.setEvent(event);
        return eventStudent;
    }

    @Before
    public void initTest() {
        eventStudent = createEntity(em);
    }

    @Test
    @Transactional
    public void createEventStudent() throws Exception {
        int databaseSizeBeforeCreate = eventStudentRepository.findAll().size();

        // Create the EventStudent
        EventStudentDTO eventStudentDTO = eventStudentMapper.toDto(eventStudent);
        restEventStudentMockMvc.perform(post("/api/event-students")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(eventStudentDTO)))
            .andExpect(status().isCreated());

        // Validate the EventStudent in the database
        List<EventStudent> eventStudentList = eventStudentRepository.findAll();
        assertThat(eventStudentList).hasSize(databaseSizeBeforeCreate + 1);
        EventStudent testEventStudent = eventStudentList.get(eventStudentList.size() - 1);
        assertThat(testEventStudent.getMinutes()).isEqualTo(DEFAULT_MINUTES);
        assertThat(testEventStudent.getGoogleEventId()).isEqualTo(DEFAULT_GOOGLE_EVENT_ID);
    }

    @Test
    @Transactional
    public void createEventStudentWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = eventStudentRepository.findAll().size();

        // Create the EventStudent with an existing ID
        eventStudent.setId(1L);
        EventStudentDTO eventStudentDTO = eventStudentMapper.toDto(eventStudent);

        // An entity with an existing ID cannot be created, so this API call must fail
        restEventStudentMockMvc.perform(post("/api/event-students")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(eventStudentDTO)))
            .andExpect(status().isBadRequest());

        // Validate the EventStudent in the database
        List<EventStudent> eventStudentList = eventStudentRepository.findAll();
        assertThat(eventStudentList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkMinutesIsRequired() throws Exception {
        int databaseSizeBeforeTest = eventStudentRepository.findAll().size();
        // set the field null
        eventStudent.setMinutes(null);

        // Create the EventStudent, which fails.
        EventStudentDTO eventStudentDTO = eventStudentMapper.toDto(eventStudent);

        restEventStudentMockMvc.perform(post("/api/event-students")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(eventStudentDTO)))
            .andExpect(status().isBadRequest());

        List<EventStudent> eventStudentList = eventStudentRepository.findAll();
        assertThat(eventStudentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllEventStudents() throws Exception {
        // Initialize the database
        eventStudentRepository.saveAndFlush(eventStudent);

        // Get all the eventStudentList
        restEventStudentMockMvc.perform(get("/api/event-students?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(eventStudent.getId().intValue())))
            .andExpect(jsonPath("$.[*].minutes").value(hasItem(DEFAULT_MINUTES)))
            .andExpect(jsonPath("$.[*].googleEventId").value(hasItem(DEFAULT_GOOGLE_EVENT_ID.toString())));
    }
    
    @Test
    @Transactional
    public void getEventStudent() throws Exception {
        // Initialize the database
        eventStudentRepository.saveAndFlush(eventStudent);

        // Get the eventStudent
        restEventStudentMockMvc.perform(get("/api/event-students/{id}", eventStudent.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(eventStudent.getId().intValue()))
            .andExpect(jsonPath("$.minutes").value(DEFAULT_MINUTES))
            .andExpect(jsonPath("$.googleEventId").value(DEFAULT_GOOGLE_EVENT_ID.toString()));
    }

    @Test
    @Transactional
    public void getAllEventStudentsByMinutesIsEqualToSomething() throws Exception {
        // Initialize the database
        eventStudentRepository.saveAndFlush(eventStudent);

        // Get all the eventStudentList where minutes equals to DEFAULT_MINUTES
        defaultEventStudentShouldBeFound("minutes.equals=" + DEFAULT_MINUTES);

        // Get all the eventStudentList where minutes equals to UPDATED_MINUTES
        defaultEventStudentShouldNotBeFound("minutes.equals=" + UPDATED_MINUTES);
    }

    @Test
    @Transactional
    public void getAllEventStudentsByMinutesIsInShouldWork() throws Exception {
        // Initialize the database
        eventStudentRepository.saveAndFlush(eventStudent);

        // Get all the eventStudentList where minutes in DEFAULT_MINUTES or UPDATED_MINUTES
        defaultEventStudentShouldBeFound("minutes.in=" + DEFAULT_MINUTES + "," + UPDATED_MINUTES);

        // Get all the eventStudentList where minutes equals to UPDATED_MINUTES
        defaultEventStudentShouldNotBeFound("minutes.in=" + UPDATED_MINUTES);
    }

    @Test
    @Transactional
    public void getAllEventStudentsByMinutesIsNullOrNotNull() throws Exception {
        // Initialize the database
        eventStudentRepository.saveAndFlush(eventStudent);

        // Get all the eventStudentList where minutes is not null
        defaultEventStudentShouldBeFound("minutes.specified=true");

        // Get all the eventStudentList where minutes is null
        defaultEventStudentShouldNotBeFound("minutes.specified=false");
    }

    @Test
    @Transactional
    public void getAllEventStudentsByMinutesIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        eventStudentRepository.saveAndFlush(eventStudent);

        // Get all the eventStudentList where minutes greater than or equals to DEFAULT_MINUTES
        defaultEventStudentShouldBeFound("minutes.greaterOrEqualThan=" + DEFAULT_MINUTES);

        // Get all the eventStudentList where minutes greater than or equals to UPDATED_MINUTES
        defaultEventStudentShouldNotBeFound("minutes.greaterOrEqualThan=" + UPDATED_MINUTES);
    }

    @Test
    @Transactional
    public void getAllEventStudentsByMinutesIsLessThanSomething() throws Exception {
        // Initialize the database
        eventStudentRepository.saveAndFlush(eventStudent);

        // Get all the eventStudentList where minutes less than or equals to DEFAULT_MINUTES
        defaultEventStudentShouldNotBeFound("minutes.lessThan=" + DEFAULT_MINUTES);

        // Get all the eventStudentList where minutes less than or equals to UPDATED_MINUTES
        defaultEventStudentShouldBeFound("minutes.lessThan=" + UPDATED_MINUTES);
    }


    @Test
    @Transactional
    public void getAllEventStudentsByGoogleEventIdIsEqualToSomething() throws Exception {
        // Initialize the database
        eventStudentRepository.saveAndFlush(eventStudent);

        // Get all the eventStudentList where googleEventId equals to DEFAULT_GOOGLE_EVENT_ID
        defaultEventStudentShouldBeFound("googleEventId.equals=" + DEFAULT_GOOGLE_EVENT_ID);

        // Get all the eventStudentList where googleEventId equals to UPDATED_GOOGLE_EVENT_ID
        defaultEventStudentShouldNotBeFound("googleEventId.equals=" + UPDATED_GOOGLE_EVENT_ID);
    }

    @Test
    @Transactional
    public void getAllEventStudentsByGoogleEventIdIsInShouldWork() throws Exception {
        // Initialize the database
        eventStudentRepository.saveAndFlush(eventStudent);

        // Get all the eventStudentList where googleEventId in DEFAULT_GOOGLE_EVENT_ID or UPDATED_GOOGLE_EVENT_ID
        defaultEventStudentShouldBeFound("googleEventId.in=" + DEFAULT_GOOGLE_EVENT_ID + "," + UPDATED_GOOGLE_EVENT_ID);

        // Get all the eventStudentList where googleEventId equals to UPDATED_GOOGLE_EVENT_ID
        defaultEventStudentShouldNotBeFound("googleEventId.in=" + UPDATED_GOOGLE_EVENT_ID);
    }

    @Test
    @Transactional
    public void getAllEventStudentsByGoogleEventIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        eventStudentRepository.saveAndFlush(eventStudent);

        // Get all the eventStudentList where googleEventId is not null
        defaultEventStudentShouldBeFound("googleEventId.specified=true");

        // Get all the eventStudentList where googleEventId is null
        defaultEventStudentShouldNotBeFound("googleEventId.specified=false");
    }

    @Test
    @Transactional
    public void getAllEventStudentsByStudentIsEqualToSomething() throws Exception {
        // Initialize the database
        Student student = StudentResourceIntTest.createEntity(em);
        em.persist(student);
        em.flush();
        eventStudent.setStudent(student);
        eventStudentRepository.saveAndFlush(eventStudent);
        Long studentId = student.getId();

        // Get all the eventStudentList where student equals to studentId
        defaultEventStudentShouldBeFound("studentId.equals=" + studentId);

        // Get all the eventStudentList where student equals to studentId + 1
        defaultEventStudentShouldNotBeFound("studentId.equals=" + (studentId + 1));
    }


    @Test
    @Transactional
    public void getAllEventStudentsByEventIsEqualToSomething() throws Exception {
        // Initialize the database
        Event event = EventResourceIntTest.createEntity(em);
        em.persist(event);
        em.flush();
        eventStudent.setEvent(event);
        eventStudentRepository.saveAndFlush(eventStudent);
        Long eventId = event.getId();

        // Get all the eventStudentList where event equals to eventId
        defaultEventStudentShouldBeFound("eventId.equals=" + eventId);

        // Get all the eventStudentList where event equals to eventId + 1
        defaultEventStudentShouldNotBeFound("eventId.equals=" + (eventId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultEventStudentShouldBeFound(String filter) throws Exception {
        restEventStudentMockMvc.perform(get("/api/event-students?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(eventStudent.getId().intValue())))
            .andExpect(jsonPath("$.[*].minutes").value(hasItem(DEFAULT_MINUTES)))
            .andExpect(jsonPath("$.[*].googleEventId").value(hasItem(DEFAULT_GOOGLE_EVENT_ID.toString())));

        // Check, that the count call also returns 1
        restEventStudentMockMvc.perform(get("/api/event-students/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultEventStudentShouldNotBeFound(String filter) throws Exception {
        restEventStudentMockMvc.perform(get("/api/event-students?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restEventStudentMockMvc.perform(get("/api/event-students/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingEventStudent() throws Exception {
        // Get the eventStudent
        restEventStudentMockMvc.perform(get("/api/event-students/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateEventStudent() throws Exception {
        // Initialize the database
        eventStudentRepository.saveAndFlush(eventStudent);

        int databaseSizeBeforeUpdate = eventStudentRepository.findAll().size();

        // Update the eventStudent
        EventStudent updatedEventStudent = eventStudentRepository.findById(eventStudent.getId()).get();
        // Disconnect from session so that the updates on updatedEventStudent are not directly saved in db
        em.detach(updatedEventStudent);
        updatedEventStudent
            .minutes(UPDATED_MINUTES)
            .googleEventId(UPDATED_GOOGLE_EVENT_ID);
        EventStudentDTO eventStudentDTO = eventStudentMapper.toDto(updatedEventStudent);

        restEventStudentMockMvc.perform(put("/api/event-students")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(eventStudentDTO)))
            .andExpect(status().isOk());

        // Validate the EventStudent in the database
        List<EventStudent> eventStudentList = eventStudentRepository.findAll();
        assertThat(eventStudentList).hasSize(databaseSizeBeforeUpdate);
        EventStudent testEventStudent = eventStudentList.get(eventStudentList.size() - 1);
        assertThat(testEventStudent.getMinutes()).isEqualTo(UPDATED_MINUTES);
        assertThat(testEventStudent.getGoogleEventId()).isEqualTo(UPDATED_GOOGLE_EVENT_ID);
    }

    @Test
    @Transactional
    public void updateNonExistingEventStudent() throws Exception {
        int databaseSizeBeforeUpdate = eventStudentRepository.findAll().size();

        // Create the EventStudent
        EventStudentDTO eventStudentDTO = eventStudentMapper.toDto(eventStudent);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEventStudentMockMvc.perform(put("/api/event-students")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(eventStudentDTO)))
            .andExpect(status().isBadRequest());

        // Validate the EventStudent in the database
        List<EventStudent> eventStudentList = eventStudentRepository.findAll();
        assertThat(eventStudentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteEventStudent() throws Exception {
        // Initialize the database
        eventStudentRepository.saveAndFlush(eventStudent);

        int databaseSizeBeforeDelete = eventStudentRepository.findAll().size();

        // Get the eventStudent
        restEventStudentMockMvc.perform(delete("/api/event-students/{id}", eventStudent.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<EventStudent> eventStudentList = eventStudentRepository.findAll();
        assertThat(eventStudentList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(EventStudent.class);
        EventStudent eventStudent1 = new EventStudent();
        eventStudent1.setId(1L);
        EventStudent eventStudent2 = new EventStudent();
        eventStudent2.setId(eventStudent1.getId());
        assertThat(eventStudent1).isEqualTo(eventStudent2);
        eventStudent2.setId(2L);
        assertThat(eventStudent1).isNotEqualTo(eventStudent2);
        eventStudent1.setId(null);
        assertThat(eventStudent1).isNotEqualTo(eventStudent2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(EventStudentDTO.class);
        EventStudentDTO eventStudentDTO1 = new EventStudentDTO();
        eventStudentDTO1.setId(1L);
        EventStudentDTO eventStudentDTO2 = new EventStudentDTO();
        assertThat(eventStudentDTO1).isNotEqualTo(eventStudentDTO2);
        eventStudentDTO2.setId(eventStudentDTO1.getId());
        assertThat(eventStudentDTO1).isEqualTo(eventStudentDTO2);
        eventStudentDTO2.setId(2L);
        assertThat(eventStudentDTO1).isNotEqualTo(eventStudentDTO2);
        eventStudentDTO1.setId(null);
        assertThat(eventStudentDTO1).isNotEqualTo(eventStudentDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(eventStudentMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(eventStudentMapper.fromId(null)).isNull();
    }
}
