package com.dyndyn.eventmanager.web.rest;

import com.dyndyn.eventmanager.EventManagerApp;

import com.dyndyn.eventmanager.domain.Authority;
import com.dyndyn.eventmanager.domain.Student;
import com.dyndyn.eventmanager.domain.EventStudent;
import com.dyndyn.eventmanager.domain.Advertisement;
import com.dyndyn.eventmanager.domain.Group;
import com.dyndyn.eventmanager.repository.StudentRepository;
import com.dyndyn.eventmanager.service.StudentService;
import com.dyndyn.eventmanager.service.dto.StudentDTO;
import com.dyndyn.eventmanager.service.mapper.StudentMapper;
import com.dyndyn.eventmanager.web.rest.errors.ExceptionTranslator;
import com.dyndyn.eventmanager.service.dto.StudentCriteria;
import com.dyndyn.eventmanager.service.StudentQueryService;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;


import static com.dyndyn.eventmanager.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the StudentResource REST controller.
 *
 * @see StudentResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = EventManagerApp.class)
@ActiveProfiles("test")
@Ignore
public class StudentResourceIntTest {

    private static final LocalDate DEFAULT_BIRTHDAY = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_BIRTHDAY = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_TELEPHONE = "AAAAAAAAAAAA";
    private static final String UPDATED_TELEPHONE = "BBBBBBBBBBBB";

    private static final String DEFAULT_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_LOGIN = "johndoe";
    private static final String UPDATED_LOGIN = "jhipster";

    private static final Long DEFAULT_ID = 1L;

    private static final String DEFAULT_PASSWORD = "passjohndoe";
    private static final String UPDATED_PASSWORD = "passjhipster";

    private static final String DEFAULT_EMAIL = "johndoe@localhost";
    private static final String UPDATED_EMAIL = "jhipster@localhost";

    private static final String DEFAULT_FIRSTNAME = "john";
    private static final String UPDATED_FIRSTNAME = "jhipsterFirstName";

    private static final String DEFAULT_LASTNAME = "doe";
    private static final String UPDATED_LASTNAME = "jhipsterLastName";

    private static final String DEFAULT_IMAGEURL = "http://placehold.it/50x50";
    private static final String UPDATED_IMAGEURL = "http://placehold.it/40x40";

    private static final String DEFAULT_LANGKEY = "en";
    private static final String UPDATED_LANGKEY = "fr";

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private StudentMapper studentMapper;
    
    @Autowired
    private StudentService studentService;

    @Autowired
    private StudentQueryService studentQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restStudentMockMvc;

    private Student student;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final StudentResource studentResource = new StudentResource(studentService, studentQueryService);
        this.restStudentMockMvc = MockMvcBuilders.standaloneSetup(studentResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Student createEntity(EntityManager em) {
        Student student = new Student()
            .birthday(DEFAULT_BIRTHDAY)
            .telephone(DEFAULT_TELEPHONE)
            .address(DEFAULT_ADDRESS);
        student.setLogin(DEFAULT_LOGIN + RandomStringUtils.randomAlphabetic(5));
        student.setPassword(RandomStringUtils.random(60));
        student.setActivated(true);
        student.setEmail(RandomStringUtils.randomAlphabetic(5) + DEFAULT_EMAIL);
        student.setFirstName(DEFAULT_FIRSTNAME);
        student.setLastName(DEFAULT_LASTNAME);
        student.setImageUrl(DEFAULT_IMAGEURL);
        student.setLangKey(DEFAULT_LANGKEY);
        student.setGroup(GroupResourceIntTest.createEntity(em));
        Authority authority = new Authority();
        authority.setName("ROLE_USER");
        student.getAuthorities().add(authority);
        return student;
    }

    @Before
    public void initTest() {
        student = createEntity(em);
    }

    @Test
    @Transactional
    public void createStudent() throws Exception {
        int databaseSizeBeforeCreate = studentRepository.findAll().size();

        // Create the Student
        StudentDTO studentDTO = studentMapper.toDto(student);
        restStudentMockMvc.perform(post("/api/students")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(studentDTO)))
            .andExpect(status().isCreated());

        // Validate the Student in the database
        List<Student> studentList = studentRepository.findAll();
        assertThat(studentList).hasSize(databaseSizeBeforeCreate + 1);
        Student testStudent = studentList.get(studentList.size() - 1);
        assertThat(testStudent.getBirthday()).isEqualTo(DEFAULT_BIRTHDAY);
        assertThat(testStudent.getTelephone()).isEqualTo(DEFAULT_TELEPHONE);
        assertThat(testStudent.getAddress()).isEqualTo(DEFAULT_ADDRESS);
    }

    @Test
    @Transactional
    public void createStudentWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = studentRepository.findAll().size();

        // Create the Student with an existing ID
        student.setId(1L);
        StudentDTO studentDTO = studentMapper.toDto(student);

        // An entity with an existing ID cannot be created, so this API call must fail
        restStudentMockMvc.perform(post("/api/students")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(studentDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Student in the database
        List<Student> studentList = studentRepository.findAll();
        assertThat(studentList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllStudents() throws Exception {
        // Initialize the database
        studentRepository.saveAndFlush(student);

        // Get all the studentList
        restStudentMockMvc.perform(get("/api/students?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(student.getId().intValue())))
            .andExpect(jsonPath("$.[*].birthday").value(hasItem(DEFAULT_BIRTHDAY.toString())))
            .andExpect(jsonPath("$.[*].telephone").value(hasItem(DEFAULT_TELEPHONE.toString())))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS.toString())));
    }
    
    @Test
    @Transactional
    public void getStudent() throws Exception {
        // Initialize the database
        studentRepository.saveAndFlush(student);

        // Get the student
        restStudentMockMvc.perform(get("/api/students/{id}", student.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(student.getId().intValue()))
            .andExpect(jsonPath("$.birthday").value(DEFAULT_BIRTHDAY.toString()))
            .andExpect(jsonPath("$.telephone").value(DEFAULT_TELEPHONE.toString()))
            .andExpect(jsonPath("$.address").value(DEFAULT_ADDRESS.toString()));
    }

    @Test
    @Transactional
    public void getAllStudentsByBirthdayIsEqualToSomething() throws Exception {
        // Initialize the database
        studentRepository.saveAndFlush(student);

        // Get all the studentList where birthday equals to DEFAULT_BIRTHDAY
        defaultStudentShouldBeFound("birthday.equals=" + DEFAULT_BIRTHDAY);

        // Get all the studentList where birthday equals to UPDATED_BIRTHDAY
        defaultStudentShouldNotBeFound("birthday.equals=" + UPDATED_BIRTHDAY);
    }

    @Test
    @Transactional
    public void getAllStudentsByBirthdayIsInShouldWork() throws Exception {
        // Initialize the database
        studentRepository.saveAndFlush(student);

        // Get all the studentList where birthday in DEFAULT_BIRTHDAY or UPDATED_BIRTHDAY
        defaultStudentShouldBeFound("birthday.in=" + DEFAULT_BIRTHDAY + "," + UPDATED_BIRTHDAY);

        // Get all the studentList where birthday equals to UPDATED_BIRTHDAY
        defaultStudentShouldNotBeFound("birthday.in=" + UPDATED_BIRTHDAY);
    }

    @Test
    @Transactional
    public void getAllStudentsByBirthdayIsNullOrNotNull() throws Exception {
        // Initialize the database
        studentRepository.saveAndFlush(student);

        // Get all the studentList where birthday is not null
        defaultStudentShouldBeFound("birthday.specified=true");

        // Get all the studentList where birthday is null
        defaultStudentShouldNotBeFound("birthday.specified=false");
    }

    @Test
    @Transactional
    public void getAllStudentsByBirthdayIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        studentRepository.saveAndFlush(student);

        // Get all the studentList where birthday greater than or equals to DEFAULT_BIRTHDAY
        defaultStudentShouldBeFound("birthday.greaterOrEqualThan=" + DEFAULT_BIRTHDAY);

        // Get all the studentList where birthday greater than or equals to UPDATED_BIRTHDAY
        defaultStudentShouldNotBeFound("birthday.greaterOrEqualThan=" + UPDATED_BIRTHDAY);
    }

    @Test
    @Transactional
    public void getAllStudentsByBirthdayIsLessThanSomething() throws Exception {
        // Initialize the database
        studentRepository.saveAndFlush(student);

        // Get all the studentList where birthday less than or equals to DEFAULT_BIRTHDAY
        defaultStudentShouldNotBeFound("birthday.lessThan=" + DEFAULT_BIRTHDAY);

        // Get all the studentList where birthday less than or equals to UPDATED_BIRTHDAY
        defaultStudentShouldBeFound("birthday.lessThan=" + UPDATED_BIRTHDAY);
    }


    @Test
    @Transactional
    public void getAllStudentsByTelephoneIsEqualToSomething() throws Exception {
        // Initialize the database
        studentRepository.saveAndFlush(student);

        // Get all the studentList where telephone equals to DEFAULT_TELEPHONE
        defaultStudentShouldBeFound("telephone.equals=" + DEFAULT_TELEPHONE);

        // Get all the studentList where telephone equals to UPDATED_TELEPHONE
        defaultStudentShouldNotBeFound("telephone.equals=" + UPDATED_TELEPHONE);
    }

    @Test
    @Transactional
    public void getAllStudentsByTelephoneIsInShouldWork() throws Exception {
        // Initialize the database
        studentRepository.saveAndFlush(student);

        // Get all the studentList where telephone in DEFAULT_TELEPHONE or UPDATED_TELEPHONE
        defaultStudentShouldBeFound("telephone.in=" + DEFAULT_TELEPHONE + "," + UPDATED_TELEPHONE);

        // Get all the studentList where telephone equals to UPDATED_TELEPHONE
        defaultStudentShouldNotBeFound("telephone.in=" + UPDATED_TELEPHONE);
    }

    @Test
    @Transactional
    public void getAllStudentsByTelephoneIsNullOrNotNull() throws Exception {
        // Initialize the database
        studentRepository.saveAndFlush(student);

        // Get all the studentList where telephone is not null
        defaultStudentShouldBeFound("telephone.specified=true");

        // Get all the studentList where telephone is null
        defaultStudentShouldNotBeFound("telephone.specified=false");
    }

    @Test
    @Transactional
    public void getAllStudentsByAddressIsEqualToSomething() throws Exception {
        // Initialize the database
        studentRepository.saveAndFlush(student);

        // Get all the studentList where address equals to DEFAULT_ADDRESS
        defaultStudentShouldBeFound("address.equals=" + DEFAULT_ADDRESS);

        // Get all the studentList where address equals to UPDATED_ADDRESS
        defaultStudentShouldNotBeFound("address.equals=" + UPDATED_ADDRESS);
    }

    @Test
    @Transactional
    public void getAllStudentsByAddressIsInShouldWork() throws Exception {
        // Initialize the database
        studentRepository.saveAndFlush(student);

        // Get all the studentList where address in DEFAULT_ADDRESS or UPDATED_ADDRESS
        defaultStudentShouldBeFound("address.in=" + DEFAULT_ADDRESS + "," + UPDATED_ADDRESS);

        // Get all the studentList where address equals to UPDATED_ADDRESS
        defaultStudentShouldNotBeFound("address.in=" + UPDATED_ADDRESS);
    }

    @Test
    @Transactional
    public void getAllStudentsByAddressIsNullOrNotNull() throws Exception {
        // Initialize the database
        studentRepository.saveAndFlush(student);

        // Get all the studentList where address is not null
        defaultStudentShouldBeFound("address.specified=true");

        // Get all the studentList where address is null
        defaultStudentShouldNotBeFound("address.specified=false");
    }

    @Test
    @Transactional
    public void getAllStudentsByEventStudentIsEqualToSomething() throws Exception {
        // Initialize the database
        EventStudent eventStudent = EventStudentResourceIntTest.createEntity(em);
        em.persist(eventStudent);
        em.flush();
        student.addEventStudent(eventStudent);
        studentRepository.saveAndFlush(student);
        Long eventStudentId = eventStudent.getId();

        // Get all the studentList where eventStudent equals to eventStudentId
        defaultStudentShouldBeFound("eventStudentId.equals=" + eventStudentId);

        // Get all the studentList where eventStudent equals to eventStudentId + 1
        defaultStudentShouldNotBeFound("eventStudentId.equals=" + (eventStudentId + 1));
    }


    @Test
    @Transactional
    public void getAllStudentsByAdvertisementIsEqualToSomething() throws Exception {
        // Initialize the database
        Advertisement advertisement = AdvertisementResourceIntTest.createEntity(em);
        em.persist(advertisement);
        em.flush();
        student.addAdvertisement(advertisement);
        studentRepository.saveAndFlush(student);
        Long advertisementId = advertisement.getId();

        // Get all the studentList where advertisement equals to advertisementId
        defaultStudentShouldBeFound("advertisementId.equals=" + advertisementId);

        // Get all the studentList where advertisement equals to advertisementId + 1
        defaultStudentShouldNotBeFound("advertisementId.equals=" + (advertisementId + 1));
    }


    @Test
    @Transactional
    public void getAllStudentsByGroupIsEqualToSomething() throws Exception {
        // Initialize the database
        Group group = GroupResourceIntTest.createEntity(em);
        em.persist(group);
        em.flush();
        student.setGroup(group);
        studentRepository.saveAndFlush(student);
        Long groupId = group.getId();

        // Get all the studentList where group equals to groupId
        defaultStudentShouldBeFound("groupId.equals=" + groupId);

        // Get all the studentList where group equals to groupId + 1
        defaultStudentShouldNotBeFound("groupId.equals=" + (groupId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultStudentShouldBeFound(String filter) throws Exception {
        restStudentMockMvc.perform(get("/api/students?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(student.getId().intValue())))
            .andExpect(jsonPath("$.[*].birthday").value(hasItem(DEFAULT_BIRTHDAY.toString())))
            .andExpect(jsonPath("$.[*].telephone").value(hasItem(DEFAULT_TELEPHONE.toString())))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS.toString())));

        // Check, that the count call also returns 1
        restStudentMockMvc.perform(get("/api/students/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultStudentShouldNotBeFound(String filter) throws Exception {
        restStudentMockMvc.perform(get("/api/students?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restStudentMockMvc.perform(get("/api/students/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingStudent() throws Exception {
        // Get the student
        restStudentMockMvc.perform(get("/api/students/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateStudent() throws Exception {
        // Initialize the database
        studentRepository.saveAndFlush(student);

        int databaseSizeBeforeUpdate = studentRepository.findAll().size();

        // Update the student
        Student updatedStudent = studentRepository.findById(student.getId()).get();
        // Disconnect from session so that the updates on updatedStudent are not directly saved in db
        em.detach(updatedStudent);
        updatedStudent
            .birthday(UPDATED_BIRTHDAY)
            .telephone(UPDATED_TELEPHONE)
            .address(UPDATED_ADDRESS);
        StudentDTO studentDTO = studentMapper.toDto(updatedStudent);

        restStudentMockMvc.perform(put("/api/students")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(studentDTO)))
            .andExpect(status().isOk());

        // Validate the Student in the database
        List<Student> studentList = studentRepository.findAll();
        assertThat(studentList).hasSize(databaseSizeBeforeUpdate);
        Student testStudent = studentList.get(studentList.size() - 1);
        assertThat(testStudent.getBirthday()).isEqualTo(UPDATED_BIRTHDAY);
        assertThat(testStudent.getTelephone()).isEqualTo(UPDATED_TELEPHONE);
        assertThat(testStudent.getAddress()).isEqualTo(UPDATED_ADDRESS);
    }

    @Test
    @Transactional
    public void updateNonExistingStudent() throws Exception {
        int databaseSizeBeforeUpdate = studentRepository.findAll().size();

        // Create the Student
        StudentDTO studentDTO = studentMapper.toDto(student);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restStudentMockMvc.perform(put("/api/students")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(studentDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Student in the database
        List<Student> studentList = studentRepository.findAll();
        assertThat(studentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteStudent() throws Exception {
        // Initialize the database
        studentRepository.saveAndFlush(student);

        int databaseSizeBeforeDelete = studentRepository.findAll().size();

        // Get the student
        restStudentMockMvc.perform(delete("/api/students/{id}", student.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Student> studentList = studentRepository.findAll();
        assertThat(studentList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Student.class);
        Student student1 = new Student();
        student1.setId(1L);
        Student student2 = new Student();
        student2.setId(student1.getId());
        assertThat(student1).isEqualTo(student2);
        student2.setId(2L);
        assertThat(student1).isNotEqualTo(student2);
        student1.setId(null);
        assertThat(student1).isNotEqualTo(student2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(StudentDTO.class);
        StudentDTO studentDTO1 = new StudentDTO();
        studentDTO1.setId(1L);
        StudentDTO studentDTO2 = new StudentDTO();
        assertThat(studentDTO1).isNotEqualTo(studentDTO2);
        studentDTO2.setId(studentDTO1.getId());
        assertThat(studentDTO1).isEqualTo(studentDTO2);
        studentDTO2.setId(2L);
        assertThat(studentDTO1).isNotEqualTo(studentDTO2);
        studentDTO1.setId(null);
        assertThat(studentDTO1).isNotEqualTo(studentDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(studentMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(studentMapper.fromId(null)).isNull();
    }
}
