/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { EventManagerTestModule } from '../../../test.module';
import { SpecialtyDeleteDialogComponent } from 'app/entities/specialty/specialty-delete-dialog.component';
import { SpecialtyService } from 'app/entities/specialty/specialty.service';

describe('Component Tests', () => {
    describe('Specialty Management Delete Component', () => {
        let comp: SpecialtyDeleteDialogComponent;
        let fixture: ComponentFixture<SpecialtyDeleteDialogComponent>;
        let service: SpecialtyService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [EventManagerTestModule],
                declarations: [SpecialtyDeleteDialogComponent]
            })
                .overrideTemplate(SpecialtyDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(SpecialtyDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SpecialtyService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it(
                'Should call delete service on confirmDelete',
                inject(
                    [],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });
});
