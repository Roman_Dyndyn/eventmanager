/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { EventManagerTestModule } from '../../../test.module';
import { AdvertisementDetailComponent } from 'app/entities/advertisement/advertisement-detail.component';
import { Advertisement } from 'app/shared/model/advertisement.model';

describe('Component Tests', () => {
    describe('Advertisement Management Detail Component', () => {
        let comp: AdvertisementDetailComponent;
        let fixture: ComponentFixture<AdvertisementDetailComponent>;
        const route = ({ data: of({ advertisement: new Advertisement(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [EventManagerTestModule],
                declarations: [AdvertisementDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(AdvertisementDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(AdvertisementDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.advertisement).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
