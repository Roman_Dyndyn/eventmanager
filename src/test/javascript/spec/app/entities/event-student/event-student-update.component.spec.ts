/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { EventManagerTestModule } from '../../../test.module';
import { EventStudentUpdateComponent } from 'app/entities/event-student/event-student-update.component';
import { EventStudentService } from 'app/entities/event-student/event-student.service';
import { EventStudent } from 'app/shared/model/event-student.model';

describe('Component Tests', () => {
    describe('EventStudent Management Update Component', () => {
        let comp: EventStudentUpdateComponent;
        let fixture: ComponentFixture<EventStudentUpdateComponent>;
        let service: EventStudentService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [EventManagerTestModule],
                declarations: [EventStudentUpdateComponent]
            })
                .overrideTemplate(EventStudentUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(EventStudentUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(EventStudentService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new EventStudent(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.eventStudent = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new EventStudent();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.eventStudent = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
