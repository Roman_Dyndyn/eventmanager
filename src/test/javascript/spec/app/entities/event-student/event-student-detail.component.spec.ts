/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { EventManagerTestModule } from '../../../test.module';
import { EventStudentDetailComponent } from 'app/entities/event-student/event-student-detail.component';
import { EventStudent } from 'app/shared/model/event-student.model';

describe('Component Tests', () => {
    describe('EventStudent Management Detail Component', () => {
        let comp: EventStudentDetailComponent;
        let fixture: ComponentFixture<EventStudentDetailComponent>;
        const route = ({ data: of({ eventStudent: new EventStudent(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [EventManagerTestModule],
                declarations: [EventStudentDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(EventStudentDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(EventStudentDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.eventStudent).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
