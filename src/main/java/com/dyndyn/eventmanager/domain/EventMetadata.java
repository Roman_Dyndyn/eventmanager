package com.dyndyn.eventmanager.domain;

import com.dyndyn.eventmanager.listener.EventMetadataUpdateListener;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A Event.
 */
@Entity
@Table(name = "event_metadata")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@EntityListeners(EventMetadataUpdateListener.class)
public class EventMetadata implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "repeat_start", nullable = false)
    private LocalDate startDate;

    @Column(name = "repeat_end", nullable = false)
    private LocalDate endDate;

    @Column(name = "`interval`", nullable = false)
    private Integer interval;

    @OneToOne(optional = false, cascade = CascadeType.ALL)
    private Event event;

    public Long getId() {
        return id;
    }

    public EventMetadata id(Long id) {
        this.id = id;
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public EventMetadata startDate(LocalDate startDate) {
        this.startDate = startDate;
        return this;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public EventMetadata endDate(LocalDate endDate) {
        this.endDate = endDate;
        return this;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public Integer getInterval() {
        return interval;
    }

    public EventMetadata interval(Integer interval) {
        this.interval = interval;
        return this;
    }

    public void setInterval(Integer interval) {
        this.interval = interval;
    }

    public Event getEvent() {
        return event;
    }

    public EventMetadata event(Event event) {
        this.event = event;
        return this;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        EventMetadata event = (EventMetadata) o;
        if (event.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), event.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "EventMetadata{" +
            "id=" + id +
            ", startDate=" + startDate +
            ", endDate=" + endDate +
            ", interval=" + interval +
            '}';
    }
}
