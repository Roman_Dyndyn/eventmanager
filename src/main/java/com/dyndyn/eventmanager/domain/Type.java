package com.dyndyn.eventmanager.domain;

public enum Type {
    STUDENT, TEACHER
}
