package com.dyndyn.eventmanager.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A Student.
 */
@Entity
@Table(name = "student")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Student extends User implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "birthday")
    private LocalDate birthday;

    @Pattern(regexp="(^$|.{12,13})", message = "telephone.size")
    @Column(name = "telephone", length = 13)
    private String telephone;

    @Column(name = "address")
    private String address;

    @OneToMany(mappedBy = "student")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<EventStudent> eventStudents = new HashSet<>();
    @ManyToMany(mappedBy = "students")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JsonIgnore
    private Set<Advertisement> advertisements = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("students")
    private Group group;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public Student id(Long id) {
        this.id = id;
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public Student birthday(LocalDate birthday) {
        this.birthday = birthday;
        return this;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getTelephone() {
        return telephone;
    }

    public Student telephone(String telephone) {
        this.telephone = telephone;
        return this;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getAddress() {
        return address;
    }

    public Student address(String address) {
        this.address = address;
        return this;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Set<EventStudent> getEventStudents() {
        return eventStudents;
    }

    public Student eventStudents(Set<EventStudent> eventStudents) {
        this.eventStudents = eventStudents;
        return this;
    }

    public Student addEventStudent(EventStudent eventStudent) {
        this.eventStudents.add(eventStudent);
        eventStudent.setStudent(this);
        return this;
    }

    public Student removeEventStudent(EventStudent eventStudent) {
        this.eventStudents.remove(eventStudent);
        eventStudent.setStudent(null);
        return this;
    }

    public void setEventStudents(Set<EventStudent> eventStudents) {
        this.eventStudents = eventStudents;
    }

    public Set<Advertisement> getAdvertisements() {
        return advertisements;
    }

    public Student advertisements(Set<Advertisement> advertisements) {
        this.advertisements = advertisements;
        return this;
    }

    public Student addAdvertisement(Advertisement advertisement) {
        this.advertisements.add(advertisement);
        advertisement.getStudents().add(this);
        return this;
    }

    public Student removeAdvertisement(Advertisement advertisement) {
        this.advertisements.remove(advertisement);
        advertisement.getStudents().remove(this);
        return this;
    }

    public void setAdvertisements(Set<Advertisement> advertisements) {
        this.advertisements = advertisements;
    }

    public Group getGroup() {
        return group;
    }

    public Student group(Group group) {
        this.group = group;
        return this;
    }

    public void setGroup(Group group) {
        this.group = group;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Student student = (Student) o;
        if (student.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), student.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Student{" +
            "id=" + id +
            ", birthday=" + birthday +
            ", telephone='" + telephone + '\'' +
            ", address='" + address + '\'' +
            ", group=" + group +
            "} " + super.toString();
    }
}
