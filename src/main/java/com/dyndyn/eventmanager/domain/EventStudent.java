package com.dyndyn.eventmanager.domain;

import com.dyndyn.eventmanager.listener.EventStudentInsertDeleteListener;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A EventStudent.
 */
@Entity
@Table(name = "event_student")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@EntityListeners(EventStudentInsertDeleteListener.class)
public class EventStudent implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "minutes", nullable = false)
    private Integer minutes;

    @Column(name = "google_event_id")
    private String googleEventId;


    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("eventStudents")
    private Student student;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("eventStudents")
    private Event event;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getMinutes() {
        return minutes;
    }

    public EventStudent minutes(Integer minutes) {
        this.minutes = minutes;
        return this;
    }

    public void setMinutes(Integer minutes) {
        this.minutes = minutes;
    }

    public String getGoogleEventId() {
        return googleEventId;
    }

    public EventStudent googleEventId(String googleEventId) {
        this.googleEventId = googleEventId;
        return this;
    }

    public void setGoogleEventId(String googleEventId) {
        this.googleEventId = googleEventId;
    }

    public Student getStudent() {
        return student;
    }

    public EventStudent student(Student student) {
        this.student = student;
        return this;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Event getEvent() {
        return event;
    }

    public EventStudent event(Event event) {
        this.event = event;
        return this;
    }

    public void setEvent(Event event) {
        this.event = event;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        EventStudent eventStudent = (EventStudent) o;
        if (eventStudent.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), eventStudent.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "EventStudent{" +
            "id=" + getId() +
            ", minutes=" + getMinutes() +
            ", googleEventId='" + getGoogleEventId() + "'" +
            "}";
    }
}
