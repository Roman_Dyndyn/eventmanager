package com.dyndyn.eventmanager.domain;

import com.dyndyn.eventmanager.listener.EventUpdateListener;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A Event.
 */
@Entity
@Table(name = "event")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@EntityListeners(EventUpdateListener.class)
public class Event implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "title", nullable = false)
    private String title;

    @Lob
    @Column(name = "description")
    private String description;

    @NotNull
    @Column(name = "start", nullable = false)
    private LocalTime start;

    @NotNull
    @Column(name = "end", nullable = false)
    private LocalTime end;

    @OneToOne(mappedBy = "event", cascade = CascadeType.ALL)
    @NotNull
    @JsonIgnoreProperties("event")
    private EventMetadata metadata;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("events")
    private Teacher teacher;

    @OneToMany(mappedBy = "event", cascade = CascadeType.ALL, orphanRemoval = true)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @NotFound(action = NotFoundAction.IGNORE)
    private Set<EventStudent> eventStudents = new HashSet<>();
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public Event title(String title) {
        this.title = title;
        return this;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public Event description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalTime getStart() {
        return start;
    }

    public Event start(LocalTime start) {
        this.start = start;
        return this;
    }

    public void setStart(LocalTime start) {
        this.start = start;
    }

    public LocalTime getEnd() {
        return end;
    }

    public Event end(LocalTime end) {
        this.end = end;
        return this;
    }

    public void setEnd(LocalTime end) {
        this.end = end;
    }

    public EventMetadata getMetadata() {
        return metadata;
    }

    public void setMetadata(EventMetadata metadata) {
        this.metadata = metadata;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public Event teacher(Teacher teacher) {
        this.teacher = teacher;
        return this;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public Set<EventStudent> getEventStudents() {
        return eventStudents;
    }

    public Event eventStudents(Set<EventStudent> eventStudents) {
        this.eventStudents = eventStudents;
        return this;
    }

    public Event addEventStudent(EventStudent eventStudent) {
        this.eventStudents.add(eventStudent);
        eventStudent.setEvent(this);
        return this;
    }

    public Event removeEventStudent(EventStudent eventStudent) {
        this.eventStudents.remove(eventStudent);
        eventStudent.setEvent(null);
        return this;
    }

    public void setEventStudents(Set<EventStudent> eventStudents) {
        this.eventStudents = eventStudents;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Event event = (Event) o;
        if (event.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), event.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Event{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            ", description='" + getDescription() + "'" +
            ", start='" + getStart() + "'" +
            ", end='" + getEnd() + "'" +
            "}";
    }
}
