/**
 * Data Access Objects used by WebSocket services.
 */
package com.dyndyn.eventmanager.web.websocket.dto;
