/**
 * View Models used by Spring MVC REST controllers.
 */
package com.dyndyn.eventmanager.web.rest.vm;
