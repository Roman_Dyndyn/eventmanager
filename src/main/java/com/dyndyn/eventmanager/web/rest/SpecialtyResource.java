package com.dyndyn.eventmanager.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.dyndyn.eventmanager.security.AuthoritiesConstants;
import com.dyndyn.eventmanager.service.SpecialtyService;
import com.dyndyn.eventmanager.service.dto.GroupDTO;
import com.dyndyn.eventmanager.web.rest.errors.BadRequestAlertException;
import com.dyndyn.eventmanager.web.rest.util.HeaderUtil;
import com.dyndyn.eventmanager.web.rest.util.PaginationUtil;
import com.dyndyn.eventmanager.service.dto.SpecialtyDTO;
import com.dyndyn.eventmanager.service.dto.SpecialtyCriteria;
import com.dyndyn.eventmanager.service.SpecialtyQueryService;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Specialty.
 */
@RestController
@RequestMapping("/api")
public class SpecialtyResource {

    private final Logger log = LoggerFactory.getLogger(SpecialtyResource.class);

    private static final String ENTITY_NAME = "specialty";

    private SpecialtyService specialtyService;

    private SpecialtyQueryService specialtyQueryService;

    public SpecialtyResource(SpecialtyService specialtyService, SpecialtyQueryService specialtyQueryService) {
        this.specialtyService = specialtyService;
        this.specialtyQueryService = specialtyQueryService;
    }

    /**
     * POST  /specialties : Create a new specialty.
     *
     * @param specialtyDTO the specialtyDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new specialtyDTO, or with status 400 (Bad Request) if the specialty has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/specialties")
    @Timed
    @PreAuthorize("hasAnyRole(\"" + AuthoritiesConstants.ADMIN + "\", \"" + AuthoritiesConstants.TEACHER + "\")")
    public ResponseEntity<SpecialtyDTO> createSpecialty(@Valid @RequestBody SpecialtyDTO specialtyDTO) throws URISyntaxException {
        log.debug("REST request to save Specialty : {}", specialtyDTO);
        if (specialtyDTO.getId() != null) {
            throw new BadRequestAlertException("A new specialty cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SpecialtyDTO result = specialtyService.save(specialtyDTO);
        return ResponseEntity.created(new URI("/api/specialties/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /specialties : Updates an existing specialty.
     *
     * @param specialtyDTO the specialtyDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated specialtyDTO,
     * or with status 400 (Bad Request) if the specialtyDTO is not valid,
     * or with status 500 (Internal Server Error) if the specialtyDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/specialties")
    @Timed
    @PreAuthorize("hasAnyRole(\"" + AuthoritiesConstants.ADMIN + "\", \"" + AuthoritiesConstants.TEACHER + "\")")
    public ResponseEntity<SpecialtyDTO> updateSpecialty(@Valid @RequestBody SpecialtyDTO specialtyDTO) throws URISyntaxException {
        log.debug("REST request to update Specialty : {}", specialtyDTO);
        if (specialtyDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        SpecialtyDTO result = specialtyService.save(specialtyDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, specialtyDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /specialties : get all the specialties.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of specialties in body
     */
    @GetMapping("/specialties")
    @Timed
    public ResponseEntity<List<SpecialtyDTO>> getAllSpecialties(SpecialtyCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Specialties by criteria: {}", criteria);
        Page<SpecialtyDTO> page = specialtyQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/specialties");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
    * GET  /specialties/count : count all the specialties.
    *
    * @param criteria the criterias which the requested entities should match
    * @return the ResponseEntity with status 200 (OK) and the count in body
    */
    @GetMapping("/specialties/count")
    @Timed
    public ResponseEntity<Long> countSpecialties(SpecialtyCriteria criteria) {
        log.debug("REST request to count Specialties by criteria: {}", criteria);
        return ResponseEntity.ok().body(specialtyQueryService.countByCriteria(criteria));
    }

    /**
     * GET  /specialties/:id : get the "id" specialty.
     *
     * @param id the id of the specialtyDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the specialtyDTO, or with status 404 (Not Found)
     */
    @GetMapping("/specialties/{id}")
    @Timed
    public ResponseEntity<SpecialtyDTO> getSpecialty(@PathVariable Long id) {
        log.debug("REST request to get Specialty : {}", id);
        Optional<SpecialtyDTO> specialtyDTO = specialtyService.findOne(id);
        return ResponseUtil.wrapOrNotFound(specialtyDTO);
    }

    /**
     * GET  /specialties/:id/groups : get the groups of "id" specialty.
     *
     * @param id the id of the specialtyDTO
     * @return the ResponseEntity with status 200 (OK) and the list of groups in body, or with status 404 (Not Found)
     */
    @GetMapping("/specialties/{id}/groups")
    @Timed
    public ResponseEntity<List<GroupDTO>> getGroupsBySpecialtyId(@PathVariable Long id) {
        log.debug("REST request to get Groups of Specialty : {}", id);
        Optional<List<GroupDTO>> groups = specialtyService.findGroupsBySpecialtyId(id);
        return ResponseUtil.wrapOrNotFound(groups);
    }

    /**
     * DELETE  /specialties/:id : delete the "id" specialty.
     *
     * @param id the id of the specialtyDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/specialties/{id}")
    @Timed
    @PreAuthorize("hasAnyRole(\"" + AuthoritiesConstants.ADMIN + "\", \"" + AuthoritiesConstants.TEACHER + "\")")
    public ResponseEntity<Void> deleteSpecialty(@PathVariable Long id) {
        log.debug("REST request to delete Specialty : {}", id);
        specialtyService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
