package com.dyndyn.eventmanager.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.dyndyn.eventmanager.security.AuthoritiesConstants;
import com.dyndyn.eventmanager.service.GroupService;
import com.dyndyn.eventmanager.service.dto.StudentDTO;
import com.dyndyn.eventmanager.web.rest.errors.BadRequestAlertException;
import com.dyndyn.eventmanager.web.rest.util.HeaderUtil;
import com.dyndyn.eventmanager.web.rest.util.PaginationUtil;
import com.dyndyn.eventmanager.service.dto.GroupDTO;
import com.dyndyn.eventmanager.service.dto.GroupCriteria;
import com.dyndyn.eventmanager.service.GroupQueryService;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Group.
 */
@RestController
@RequestMapping("/api")
public class GroupResource {

    private final Logger log = LoggerFactory.getLogger(GroupResource.class);

    private static final String ENTITY_NAME = "group";

    private GroupService groupService;

    private GroupQueryService groupQueryService;

    public GroupResource(GroupService groupService, GroupQueryService groupQueryService) {
        this.groupService = groupService;
        this.groupQueryService = groupQueryService;
    }

    /**
     * POST  /groups : Create a new group.
     *
     * @param groupDTO the groupDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new groupDTO, or with status 400 (Bad Request) if the group has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/groups")
    @Timed
    @PreAuthorize("hasAnyRole(\"" + AuthoritiesConstants.ADMIN + "\", \"" + AuthoritiesConstants.TEACHER + "\")")
    public ResponseEntity<GroupDTO> createGroup(@Valid @RequestBody GroupDTO groupDTO) throws URISyntaxException {
        log.debug("REST request to save Group : {}", groupDTO);
        if (groupDTO.getId() != null) {
            throw new BadRequestAlertException("A new group cannot already have an ID", ENTITY_NAME, "idexists");
        }
        GroupDTO result = groupService.save(groupDTO);
        return ResponseEntity.created(new URI("/api/groups/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /groups : Updates an existing group.
     *
     * @param groupDTO the groupDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated groupDTO,
     * or with status 400 (Bad Request) if the groupDTO is not valid,
     * or with status 500 (Internal Server Error) if the groupDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/groups")
    @Timed
    @PreAuthorize("hasAnyRole(\"" + AuthoritiesConstants.ADMIN + "\", \"" + AuthoritiesConstants.TEACHER + "\")")
    public ResponseEntity<GroupDTO> updateGroup(@Valid @RequestBody GroupDTO groupDTO) throws URISyntaxException {
        log.debug("REST request to update Group : {}", groupDTO);
        if (groupDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        GroupDTO result = groupService.save(groupDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, groupDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /groups : get all the groups.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of groups in body
     */
    @GetMapping("/groups")
    @Timed
    public ResponseEntity<List<GroupDTO>> getAllGroups(GroupCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Groups by criteria: {}", criteria);
        Page<GroupDTO> page = groupQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/groups");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
    * GET  /groups/count : count all the groups.
    *
    * @param criteria the criterias which the requested entities should match
    * @return the ResponseEntity with status 200 (OK) and the count in body
    */
    @GetMapping("/groups/count")
    @Timed
    public ResponseEntity<Long> countGroups(GroupCriteria criteria) {
        log.debug("REST request to count Groups by criteria: {}", criteria);
        return ResponseEntity.ok().body(groupQueryService.countByCriteria(criteria));
    }

    /**
     * GET  /groups/:id : get the "id" group.
     *
     * @param id the id of the groupDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the groupDTO, or with status 404 (Not Found)
     */
    @GetMapping("/groups/{id}")
    @Timed
    public ResponseEntity<GroupDTO> getGroup(@PathVariable Long id) {
        log.debug("REST request to get Group : {}", id);
        Optional<GroupDTO> groupDTO = groupService.findOne(id);
        return ResponseUtil.wrapOrNotFound(groupDTO);
    }

    /**
     * GET  /groups/:id/students : get the students of  "id" group.
     *
     * @param id the id of the groupDTO
     * @return the ResponseEntity with status 200 (OK) and the list of students in body, or with status 404 (Not Found)
     */
    @GetMapping("/groups/{id}/students")
    @Timed
    public ResponseEntity<List<StudentDTO>> getStudentsByGroupId(@PathVariable Long id) {
        log.debug("REST request to get Students of Group : {}", id);
        Optional<List<StudentDTO>> students = groupService.findStudentsByGroupId(id);
        return ResponseUtil.wrapOrNotFound(students);
    }

    /**
     * DELETE  /groups/:id : delete the "id" group.
     *
     * @param id the id of the groupDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/groups/{id}")
    @Timed
    @PreAuthorize("hasAnyRole(\"" + AuthoritiesConstants.ADMIN + "\", \"" + AuthoritiesConstants.TEACHER + "\")")
    public ResponseEntity<Void> deleteGroup(@PathVariable Long id) {
        log.debug("REST request to delete Group : {}", id);
        groupService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
