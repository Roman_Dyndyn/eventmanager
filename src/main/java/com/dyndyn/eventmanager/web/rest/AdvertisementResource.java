package com.dyndyn.eventmanager.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.dyndyn.eventmanager.domain.Student;
import com.dyndyn.eventmanager.domain.User;
import com.dyndyn.eventmanager.repository.UserRepository;
import com.dyndyn.eventmanager.security.AuthoritiesConstants;
import com.dyndyn.eventmanager.security.SecurityUtils;
import com.dyndyn.eventmanager.service.AdvertisementService;
import com.dyndyn.eventmanager.web.rest.errors.BadRequestAlertException;
import com.dyndyn.eventmanager.web.rest.errors.EmailAlreadyUsedException;
import com.dyndyn.eventmanager.web.rest.errors.InternalServerErrorException;
import com.dyndyn.eventmanager.web.rest.util.HeaderUtil;
import com.dyndyn.eventmanager.web.rest.util.PaginationUtil;
import com.dyndyn.eventmanager.service.dto.AdvertisementDTO;
import com.dyndyn.eventmanager.service.dto.AdvertisementCriteria;
import com.dyndyn.eventmanager.service.AdvertisementQueryService;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Advertisement.
 */
@RestController
@RequestMapping("/api")
public class AdvertisementResource {

    private final Logger log = LoggerFactory.getLogger(AdvertisementResource.class);

    private static final String ENTITY_NAME = "advertisement";

    private final UserRepository userRepository;

    private AdvertisementService advertisementService;

    private AdvertisementQueryService advertisementQueryService;

    public AdvertisementResource(AdvertisementService advertisementService, AdvertisementQueryService advertisementQueryService,
                                 UserRepository userRepository) {
        this.advertisementService = advertisementService;
        this.advertisementQueryService = advertisementQueryService;
        this.userRepository = userRepository;
    }

    /**
     * POST  /advertisements : Create a new advertisement.
     *
     * @param advertisementDTO the advertisementDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new advertisementDTO, or with status 400 (Bad Request) if the advertisement has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/advertisements")
    @Timed
    @PreAuthorize("hasAnyRole(\"" + AuthoritiesConstants.ADMIN + "\", \"" + AuthoritiesConstants.TEACHER + "\")")
    public ResponseEntity<AdvertisementDTO> createAdvertisement(@Valid @RequestBody AdvertisementDTO advertisementDTO) throws URISyntaxException {
        log.debug("REST request to save Advertisement : {}", advertisementDTO);
        if (advertisementDTO.getId() != null) {
            throw new BadRequestAlertException("A new advertisement cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AdvertisementDTO result = advertisementService.save(advertisementDTO);
        return ResponseEntity.created(new URI("/api/advertisements/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /advertisements : Updates an existing advertisement.
     *
     * @param advertisementDTO the advertisementDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated advertisementDTO,
     * or with status 400 (Bad Request) if the advertisementDTO is not valid,
     * or with status 500 (Internal Server Error) if the advertisementDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/advertisements")
    @Timed
    @PreAuthorize("hasAnyRole(\"" + AuthoritiesConstants.ADMIN + "\", \"" + AuthoritiesConstants.TEACHER + "\")")
    public ResponseEntity<AdvertisementDTO> updateAdvertisement(@Valid @RequestBody AdvertisementDTO advertisementDTO) throws URISyntaxException {
        log.debug("REST request to update Advertisement : {}", advertisementDTO);
        if (advertisementDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AdvertisementDTO result = advertisementService.save(advertisementDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, advertisementDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /advertisements : get all the advertisements.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of advertisements in body
     */
    @GetMapping("/advertisements")
    @Timed
    public ResponseEntity<List<AdvertisementDTO>> getAllAdvertisements(AdvertisementCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Advertisements by criteria: {}", criteria);
        final String userLogin = SecurityUtils.getCurrentUserLogin().orElseThrow(() -> new InternalServerErrorException("Current user login not found"));
        Optional<User> user = userRepository.findOneByLogin(userLogin);
        if (!user.isPresent()) {
            throw new InternalServerErrorException("User could not be found");
        } else if (user.get() instanceof Student) {
            criteria.setStudentId(user.get().getId());
        }

        Page<AdvertisementDTO> page = advertisementQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/advertisements");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
    * GET  /advertisements/count : count all the advertisements.
    *
    * @param criteria the criterias which the requested entities should match
    * @return the ResponseEntity with status 200 (OK) and the count in body
    */
    @GetMapping("/advertisements/count")
    @Timed
    public ResponseEntity<Long> countAdvertisements(AdvertisementCriteria criteria) {
        log.debug("REST request to count Advertisements by criteria: {}", criteria);
        return ResponseEntity.ok().body(advertisementQueryService.countByCriteria(criteria));
    }

    /**
     * GET  /advertisements/:id : get the "id" advertisement.
     *
     * @param id the id of the advertisementDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the advertisementDTO, or with status 404 (Not Found)
     */
    @GetMapping("/advertisements/{id}")
    @Timed
    public ResponseEntity<AdvertisementDTO> getAdvertisement(@PathVariable Long id) {
        log.debug("REST request to get Advertisement : {}", id);
        Optional<AdvertisementDTO> advertisementDTO = advertisementService.findOne(id);
        return ResponseUtil.wrapOrNotFound(advertisementDTO);
    }

    /**
     * DELETE  /advertisements/:id : delete the "id" advertisement.
     *
     * @param id the id of the advertisementDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/advertisements/{id}")
    @Timed
    @PreAuthorize("hasAnyRole(\"" + AuthoritiesConstants.ADMIN + "\", \"" + AuthoritiesConstants.TEACHER + "\")")
    public ResponseEntity<Void> deleteAdvertisement(@PathVariable Long id) {
        log.debug("REST request to delete Advertisement : {}", id);
        advertisementService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
