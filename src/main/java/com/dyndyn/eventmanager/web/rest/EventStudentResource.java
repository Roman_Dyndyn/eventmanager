package com.dyndyn.eventmanager.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.dyndyn.eventmanager.domain.Student;
import com.dyndyn.eventmanager.domain.User;
import com.dyndyn.eventmanager.repository.UserRepository;
import com.dyndyn.eventmanager.security.SecurityUtils;
import com.dyndyn.eventmanager.service.EventStudentService;
import com.dyndyn.eventmanager.web.rest.errors.BadRequestAlertException;
import com.dyndyn.eventmanager.web.rest.errors.InternalServerErrorException;
import com.dyndyn.eventmanager.web.rest.util.HeaderUtil;
import com.dyndyn.eventmanager.web.rest.util.PaginationUtil;
import com.dyndyn.eventmanager.service.dto.EventStudentDTO;
import com.dyndyn.eventmanager.service.dto.EventStudentCriteria;
import com.dyndyn.eventmanager.service.EventStudentQueryService;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing EventStudent.
 */
@RestController
@RequestMapping("/api")
public class EventStudentResource {

    private final Logger log = LoggerFactory.getLogger(EventStudentResource.class);

    private static final String ENTITY_NAME = "eventStudent";

    private EventStudentService eventStudentService;

    private EventStudentQueryService eventStudentQueryService;

    private final UserRepository userRepository;

    public EventStudentResource(EventStudentService eventStudentService, EventStudentQueryService eventStudentQueryService,
                                UserRepository userRepository) {
        this.eventStudentService = eventStudentService;
        this.eventStudentQueryService = eventStudentQueryService;
        this.userRepository = userRepository;
    }

    /**
     * POST  /event-students : Create a new eventStudent.
     *
     * @param eventStudentDTO the eventStudentDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new eventStudentDTO, or with status 400 (Bad Request) if the eventStudent has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/event-students")
    @Timed
    public ResponseEntity<EventStudentDTO> createEventStudent(@Valid @RequestBody EventStudentDTO eventStudentDTO) throws URISyntaxException {
        log.debug("REST request to save EventStudent : {}", eventStudentDTO);
        if (eventStudentDTO.getId() != null) {
            throw new BadRequestAlertException("A new eventStudent cannot already have an ID", ENTITY_NAME, "idexists");
        }
        EventStudentDTO result = eventStudentService.save(eventStudentDTO);
        return ResponseEntity.created(new URI("/api/event-students/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /event-students : Updates an existing eventStudent.
     *
     * @param eventStudentDTO the eventStudentDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated eventStudentDTO,
     * or with status 400 (Bad Request) if the eventStudentDTO is not valid,
     * or with status 500 (Internal Server Error) if the eventStudentDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/event-students")
    @Timed
    public ResponseEntity<EventStudentDTO> updateEventStudent(@Valid @RequestBody EventStudentDTO eventStudentDTO) throws URISyntaxException {
        log.debug("REST request to update EventStudent : {}", eventStudentDTO);
        if (eventStudentDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        EventStudentDTO result = eventStudentService.save(eventStudentDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, eventStudentDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /event-students : get all the eventStudents.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of eventStudents in body
     */
    @GetMapping("/event-students")
    @Timed
    public ResponseEntity<List<EventStudentDTO>> getAllEventStudents(EventStudentCriteria criteria, Pageable pageable) {
        log.debug("REST request to get EventStudents by criteria: {}", criteria);
        final String userLogin = SecurityUtils.getCurrentUserLogin().orElseThrow(() -> new InternalServerErrorException("Current user login not found"));
        Optional<User> user = userRepository.findOneByLogin(userLogin);
        if (!user.isPresent()) {
            throw new InternalServerErrorException("User could not be found");
        } else if (user.get() instanceof Student) {
            criteria.setStudentId(user.get().getId());
        }
        Page<EventStudentDTO> page = eventStudentQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/event-students");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
    * GET  /event-students/count : count all the eventStudents.
    *
    * @param criteria the criterias which the requested entities should match
    * @return the ResponseEntity with status 200 (OK) and the count in body
    */
    @GetMapping("/event-students/count")
    @Timed
    public ResponseEntity<Long> countEventStudents(EventStudentCriteria criteria) {
        log.debug("REST request to count EventStudents by criteria: {}", criteria);
        return ResponseEntity.ok().body(eventStudentQueryService.countByCriteria(criteria));
    }

    /**
     * GET  /event-students/:id : get the "id" eventStudent.
     *
     * @param id the id of the eventStudentDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the eventStudentDTO, or with status 404 (Not Found)
     */
    @GetMapping("/event-students/{id}")
    @Timed
    public ResponseEntity<EventStudentDTO> getEventStudent(@PathVariable Long id) {
        log.debug("REST request to get EventStudent : {}", id);
        Optional<EventStudentDTO> eventStudentDTO = eventStudentService.findOne(id);
        return ResponseUtil.wrapOrNotFound(eventStudentDTO);
    }

    /**
     * DELETE  /event-students/:id : delete the "id" eventStudent.
     *
     * @param id the id of the eventStudentDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/event-students/{id}")
    @Timed
    public ResponseEntity<Void> deleteEventStudent(@PathVariable Long id) {
        log.debug("REST request to delete EventStudent : {}", id);
        eventStudentService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
