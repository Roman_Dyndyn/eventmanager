package com.dyndyn.eventmanager.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.dyndyn.eventmanager.security.AuthoritiesConstants;
import com.dyndyn.eventmanager.service.FacultyService;
import com.dyndyn.eventmanager.service.dto.SpecialtyDTO;
import com.dyndyn.eventmanager.web.rest.errors.BadRequestAlertException;
import com.dyndyn.eventmanager.web.rest.util.HeaderUtil;
import com.dyndyn.eventmanager.web.rest.util.PaginationUtil;
import com.dyndyn.eventmanager.service.dto.FacultyDTO;
import com.dyndyn.eventmanager.service.dto.FacultyCriteria;
import com.dyndyn.eventmanager.service.FacultyQueryService;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Faculty.
 */
@RestController
@RequestMapping("/api")
public class FacultyResource {

    private final Logger log = LoggerFactory.getLogger(FacultyResource.class);

    private static final String ENTITY_NAME = "faculty";

    private FacultyService facultyService;

    private FacultyQueryService facultyQueryService;

    public FacultyResource(FacultyService facultyService, FacultyQueryService facultyQueryService) {
        this.facultyService = facultyService;
        this.facultyQueryService = facultyQueryService;
    }

    /**
     * POST  /faculties : Create a new faculty.
     *
     * @param facultyDTO the facultyDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new facultyDTO, or with status 400 (Bad Request) if the faculty has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/faculties")
    @Timed
    @PreAuthorize("hasAnyRole(\"" + AuthoritiesConstants.ADMIN + "\", \"" + AuthoritiesConstants.TEACHER + "\")")
    public ResponseEntity<FacultyDTO> createFaculty(@Valid @RequestBody FacultyDTO facultyDTO) throws URISyntaxException {
        log.debug("REST request to save Faculty : {}", facultyDTO);
        if (facultyDTO.getId() != null) {
            throw new BadRequestAlertException("A new faculty cannot already have an ID", ENTITY_NAME, "idexists");
        }
        FacultyDTO result = facultyService.save(facultyDTO);
        return ResponseEntity.created(new URI("/api/faculties/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /faculties : Updates an existing faculty.
     *
     * @param facultyDTO the facultyDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated facultyDTO,
     * or with status 400 (Bad Request) if the facultyDTO is not valid,
     * or with status 500 (Internal Server Error) if the facultyDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/faculties")
    @Timed
    @PreAuthorize("hasAnyRole(\"" + AuthoritiesConstants.ADMIN + "\", \"" + AuthoritiesConstants.TEACHER + "\")")
    public ResponseEntity<FacultyDTO> updateFaculty(@Valid @RequestBody FacultyDTO facultyDTO) throws URISyntaxException {
        log.debug("REST request to update Faculty : {}", facultyDTO);
        if (facultyDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        FacultyDTO result = facultyService.save(facultyDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, facultyDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /faculties : get all the faculties.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of faculties in body
     */
    @GetMapping("/faculties")
    @Timed
    public ResponseEntity<List<FacultyDTO>> getAllFaculties(FacultyCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Faculties by criteria: {}", criteria);
        Page<FacultyDTO> page = facultyQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/faculties");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
    * GET  /faculties/count : count all the faculties.
    *
    * @param criteria the criterias which the requested entities should match
    * @return the ResponseEntity with status 200 (OK) and the count in body
    */
    @GetMapping("/faculties/count")
    @Timed
    public ResponseEntity<Long> countFaculties(FacultyCriteria criteria) {
        log.debug("REST request to count Faculties by criteria: {}", criteria);
        return ResponseEntity.ok().body(facultyQueryService.countByCriteria(criteria));
    }

    /**
     * GET  /faculties/:id : get the "id" faculty.
     *
     * @param id the id of the facultyDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the facultyDTO, or with status 404 (Not Found)
     */
    @GetMapping("/faculties/{id}")
    @Timed
    public ResponseEntity<FacultyDTO> getFaculty(@PathVariable Long id) {
        log.debug("REST request to get Faculty : {}", id);
        Optional<FacultyDTO> facultyDTO = facultyService.findOne(id);
        return ResponseUtil.wrapOrNotFound(facultyDTO);
    }

    /**
     * GET  /faculties/:id/specialties : get the specialties of "id" faculty.
     *
     * @param id the id of the facultyDTO
     * @return the ResponseEntity with status 200 (OK) and the list of specialties in body, or with status 404 (Not Found)
     */
    @GetMapping("/faculties/{id}/specialties")
    @Timed
    public ResponseEntity<List<SpecialtyDTO>> getFacultySpecialties(@PathVariable Long id) {
        log.debug("REST request to get Specialties of Faculty : {}", id);
        Optional<List<SpecialtyDTO>> specialties = facultyService.findSpecialtiesByFacultyId(id);
        return ResponseUtil.wrapOrNotFound(specialties);
    }

    /**
     * DELETE  /faculties/:id : delete the "id" faculty.
     *
     * @param id the id of the facultyDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/faculties/{id}")
    @Timed
    @PreAuthorize("hasAnyRole(\"" + AuthoritiesConstants.ADMIN + "\", \"" + AuthoritiesConstants.TEACHER + "\")")
    public ResponseEntity<Void> deleteFaculty(@PathVariable Long id) {
        log.debug("REST request to delete Faculty : {}", id);
        facultyService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
