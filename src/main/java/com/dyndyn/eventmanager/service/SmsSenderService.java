package com.dyndyn.eventmanager.service;

import com.dyndyn.eventmanager.config.ApplicationProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

import javax.annotation.PostConstruct;

@Service
@Profile("!test")
public class SmsSenderService {

    private final Logger log = LoggerFactory.getLogger(SmsSenderService.class);

    private final ApplicationProperties.Twilio properties;

    public SmsSenderService(ApplicationProperties applicationProperties) {
        this.properties = applicationProperties.getTwilio();
    }

    @PostConstruct
    public void init() {
        Twilio.init(properties.getAccountSid(), properties.getAuthToken());
    }

    public void sendSMS(String to, String message) {
        if (properties.isEnabled()) {
            Message sms = Message
                .creator(new PhoneNumber(to), // to
                    new PhoneNumber(properties.getPhone()), // from
                    message)
                .create();

            log.info("sending sms to {}, sid {}", to, sms.getSid());
        } else {
            log.info("Twilio is disabled");
        }
    }

}
