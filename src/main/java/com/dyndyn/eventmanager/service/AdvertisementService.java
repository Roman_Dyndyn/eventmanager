package com.dyndyn.eventmanager.service;

import com.dyndyn.eventmanager.domain.Advertisement;
import com.dyndyn.eventmanager.repository.AdvertisementRepository;
import com.dyndyn.eventmanager.service.dto.AdvertisementDTO;
import com.dyndyn.eventmanager.service.mapper.AdvertisementMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing Advertisement.
 */
@Service
@Transactional
public class AdvertisementService {

    private final Logger log = LoggerFactory.getLogger(AdvertisementService.class);

    private AdvertisementRepository advertisementRepository;

    private AdvertisementMapper advertisementMapper;

    public AdvertisementService(AdvertisementRepository advertisementRepository, AdvertisementMapper advertisementMapper) {
        this.advertisementRepository = advertisementRepository;
        this.advertisementMapper = advertisementMapper;
    }

    /**
     * Save a advertisement.
     *
     * @param advertisementDTO the entity to save
     * @return the persisted entity
     */
    public AdvertisementDTO save(AdvertisementDTO advertisementDTO) {
        log.debug("Request to save Advertisement : {}", advertisementDTO);

        Advertisement advertisement = advertisementMapper.toEntity(advertisementDTO);
        advertisement = advertisementRepository.save(advertisement);
        return advertisementMapper.toDto(advertisement);
    }

    /**
     * Get all the advertisements.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<AdvertisementDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Advertisements");
        return advertisementRepository.findAll(pageable)
            .map(advertisementMapper::toDto);
    }

    /**
     * Get all the Advertisement with eager load of many-to-many relationships.
     *
     * @return the list of entities
     */
    public Page<AdvertisementDTO> findAllWithEagerRelationships(Pageable pageable) {
        return advertisementRepository.findAllWithEagerRelationships(pageable).map(advertisementMapper::toDto);
    }
    

    /**
     * Get one advertisement by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<AdvertisementDTO> findOne(Long id) {
        log.debug("Request to get Advertisement : {}", id);
        return advertisementRepository.findOneWithEagerRelationships(id)
            .map(advertisementMapper::toDto);
    }

    /**
     * Delete the advertisement by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Advertisement : {}", id);
        advertisementRepository.deleteById(id);
    }
}
