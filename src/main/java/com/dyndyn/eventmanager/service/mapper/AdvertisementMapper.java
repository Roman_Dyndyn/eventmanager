package com.dyndyn.eventmanager.service.mapper;

import com.dyndyn.eventmanager.domain.*;
import com.dyndyn.eventmanager.service.dto.AdvertisementDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Advertisement and its DTO AdvertisementDTO.
 */
@Mapper(componentModel = "spring", uses = {TeacherMapper.class, StudentMapper.class})
public interface AdvertisementMapper extends EntityMapper<AdvertisementDTO, Advertisement> {

    @Mapping(source = "teacher.id", target = "teacherId")
    @Mapping(expression  = "java(advertisement.getTeacher().getFirstName() + \" \" + advertisement.getTeacher().getLastName())", target = "teacherName")
    AdvertisementDTO toDto(Advertisement advertisement);

    @Mapping(source = "teacherId", target = "teacher")
    Advertisement toEntity(AdvertisementDTO advertisementDTO);

    default Advertisement fromId(Long id) {
        if (id == null) {
            return null;
        }
        Advertisement advertisement = new Advertisement();
        advertisement.setId(id);
        return advertisement;
    }
}
