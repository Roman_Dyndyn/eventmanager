package com.dyndyn.eventmanager.service.mapper;

import com.dyndyn.eventmanager.domain.*;
import com.dyndyn.eventmanager.service.dto.TeacherDTO;

import org.mapstruct.*;

import java.util.Set;
import java.util.stream.Collectors;

/**
 * Mapper for the entity Teacher and its DTO TeacherDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface TeacherMapper extends EntityMapper<TeacherDTO, Teacher> {

    @Mapping(expression  = "java(teacher.getAuthorities().stream().map(com.dyndyn.eventmanager.domain.Authority::getName)" +
        ".collect(java.util.stream.Collectors.toSet()))", target = "authorities")
    TeacherDTO toDto(Teacher teacher);

    @Mapping(target = "events", ignore = true)
    @Mapping(target = "advertisements", ignore = true)
    @Mapping(expression  = "java(this.authoritiesFromStrings(teacherDTO.getAuthorities()))", target = "authorities")
    Teacher toEntity(TeacherDTO teacherDTO);

    default Teacher fromId(Long id) {
        if (id == null) {
            return null;
        }
        Teacher teacher = new Teacher();
        teacher.setId(id);
        return teacher;
    }

    default Set<Authority> authoritiesFromStrings(Set<String> strings) {
        return strings.stream().map(string -> {
            Authority auth = new Authority();
            auth.setName(string);
            return auth;
        }).collect(Collectors.toSet());
    }
}
