package com.dyndyn.eventmanager.service.mapper;

import com.dyndyn.eventmanager.domain.*;
import com.dyndyn.eventmanager.service.dto.EventStudentDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity EventStudent and its DTO EventStudentDTO.
 */
@Mapper(componentModel = "spring", uses = {StudentMapper.class, EventMapper.class})
public interface EventStudentMapper extends EntityMapper<EventStudentDTO, EventStudent> {

    @Mapping(source = "student.id", target = "studentId")
    @Mapping(expression  = "java(eventStudent.getStudent().getFirstName() + \" \" + eventStudent.getStudent().getLastName())", target = "studentName")
    @Mapping(source = "event.id", target = "eventId")
    @Mapping(source = "event.title", target = "eventTitle")
    EventStudentDTO toDto(EventStudent eventStudent);

    @Mapping(source = "studentId", target = "student")
    @Mapping(source = "eventId", target = "event")
    EventStudent toEntity(EventStudentDTO eventStudentDTO);

    default EventStudent fromId(Long id) {
        if (id == null) {
            return null;
        }
        EventStudent eventStudent = new EventStudent();
        eventStudent.setId(id);
        return eventStudent;
    }
}
