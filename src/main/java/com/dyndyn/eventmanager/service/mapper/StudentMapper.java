package com.dyndyn.eventmanager.service.mapper;


import com.dyndyn.eventmanager.domain.Authority;
import com.dyndyn.eventmanager.domain.Student;
import com.dyndyn.eventmanager.service.dto.StudentDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.Set;
import java.util.stream.Collectors;


/**
 * Mapper for the entity Student and its DTO StudentDTO.
 */
@Mapper(componentModel = "spring", uses = {GroupMapper.class})
public interface StudentMapper extends EntityMapper<StudentDTO, Student> {

    @Mapping(source = "group.id", target = "groupId")
    @Mapping(source = "group.name", target = "groupName")
    @Mapping(expression  = "java(student.getAuthorities().stream().map(com.dyndyn.eventmanager.domain.Authority::getName)" +
        ".collect(java.util.stream.Collectors.toSet()))", target = "authorities")
    StudentDTO toDto(Student student);

    @Mapping(target = "eventStudents", ignore = true)
    @Mapping(target = "advertisements", ignore = true)
    @Mapping(source = "groupId", target = "group")
    @Mapping(expression  = "java(this.authoritiesFromStrings(studentDTO.getAuthorities()))", target = "authorities")
    Student toEntity(StudentDTO studentDTO);

    default Student fromId(Long id) {
        if (id == null) {
            return null;
        }
        Student student = new Student();
        student.setId(id);
        return student;
    }

    default Set<Authority> authoritiesFromStrings(Set<String> strings) {
        return strings.stream().map(string -> {
            Authority auth = new Authority();
            auth.setName(string);
            return auth;
        }).collect(Collectors.toSet());
    }
}
