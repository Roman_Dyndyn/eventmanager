package com.dyndyn.eventmanager.service.mapper;

import com.dyndyn.eventmanager.domain.*;
import com.dyndyn.eventmanager.service.dto.SpecialtyDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Specialty and its DTO SpecialtyDTO.
 */
@Mapper(componentModel = "spring", uses = {FacultyMapper.class})
public interface SpecialtyMapper extends EntityMapper<SpecialtyDTO, Specialty> {

    @Mapping(source = "faculty.id", target = "facultyId")
    @Mapping(source = "faculty.name", target = "facultyName")
    SpecialtyDTO toDto(Specialty specialty);

    @Mapping(source = "facultyId", target = "faculty")
    @Mapping(target = "groups", ignore = true)
    Specialty toEntity(SpecialtyDTO specialtyDTO);

    default Specialty fromId(Long id) {
        if (id == null) {
            return null;
        }
        Specialty specialty = new Specialty();
        specialty.setId(id);
        return specialty;
    }
}
