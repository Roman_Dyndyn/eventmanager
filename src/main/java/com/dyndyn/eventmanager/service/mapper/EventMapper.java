package com.dyndyn.eventmanager.service.mapper;

import com.dyndyn.eventmanager.domain.Event;
import com.dyndyn.eventmanager.domain.EventMetadata;
import com.dyndyn.eventmanager.domain.EventStudent;
import com.dyndyn.eventmanager.domain.Student;
import com.dyndyn.eventmanager.service.dto.EventDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.HashSet;
import java.util.Set;

/**
 * Mapper for the entity Event and its DTO EventDTO.
 */
@Mapper(componentModel = "spring", uses = {TeacherMapper.class})
public interface EventMapper extends EntityMapper<EventDTO, Event> {

    @Mapping(source = "teacher.id", target = "teacherId")
    @Mapping(source = "metadata.id", target = "metadataId")
    @Mapping(source = "metadata.startDate", target = "repeatStart")
    @Mapping(source = "metadata.endDate", target = "repeatEnd")
    @Mapping(source = "metadata.interval", target = "interval")
    @Mapping(expression  = "java(event.getTeacher().getFirstName() + \" \" + event.getTeacher().getLastName())", target = "teacherName")
    @Mapping(expression  = "java(event.getEventStudents().stream().map(eventStudent -> eventStudent.getStudent().getId()).collect(java.util.stream.Collectors.toList()))", target = "studentIds")
    EventDTO toDto(Event event);

    @Mapping(source = "teacherId", target = "teacher")
    @Mapping(expression  = "java(studentIdsListToEventStudentSet(eventDTO, event))", target = "eventStudents")
    @Mapping(expression  = "java(metadata(eventDTO, event))", target = "metadata")
    Event toEntity(EventDTO eventDTO);

    default Event fromId(Long id) {
        if (id == null) {
            return null;
        }
        Event event = new Event();
        event.setId(id);
        return event;
    }

    default Set<EventStudent> studentIdsListToEventStudentSet(EventDTO eventDTO, Event event) {
        if ( eventDTO.getStudentIds() == null ) {
            return null;
        }

        Set<EventStudent> set = new HashSet<>( Math.max( (int) ( eventDTO.getStudentIds().size() / .75f ) + 1, 16 ) );
        for ( Long studentId : eventDTO.getStudentIds() ) {
            set.add(new EventStudent().student(new Student().id(studentId)).event(event));
        }

        return set;
    }

    default EventMetadata metadata(EventDTO eventDTO, Event event) {
        return new EventMetadata().startDate(eventDTO.getRepeatStart()).endDate(eventDTO.getRepeatEnd())
            .event(event).id(eventDTO.getMetadataId()).interval(eventDTO.getInterval());
    }
}
