package com.dyndyn.eventmanager.service.mapper;

import com.dyndyn.eventmanager.domain.*;
import com.dyndyn.eventmanager.service.dto.GroupDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Group and its DTO GroupDTO.
 */
@Mapper(componentModel = "spring", uses = {SpecialtyMapper.class})
public interface GroupMapper extends EntityMapper<GroupDTO, Group> {

    @Mapping(source = "specialty.id", target = "specialtyId")
    @Mapping(source = "specialty.name", target = "specialtyName")
    GroupDTO toDto(Group group);

    @Mapping(source = "specialtyId", target = "specialty")
    @Mapping(target = "students", ignore = true)
    Group toEntity(GroupDTO groupDTO);

    default Group fromId(Long id) {
        if (id == null) {
            return null;
        }
        Group group = new Group();
        group.setId(id);
        return group;
    }
}
