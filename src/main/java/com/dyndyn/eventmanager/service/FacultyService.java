package com.dyndyn.eventmanager.service;

import com.dyndyn.eventmanager.domain.Faculty;
import com.dyndyn.eventmanager.repository.FacultyRepository;
import com.dyndyn.eventmanager.service.dto.FacultyDTO;
import com.dyndyn.eventmanager.service.dto.SpecialtyDTO;
import com.dyndyn.eventmanager.service.mapper.FacultyMapper;
import com.dyndyn.eventmanager.service.mapper.SpecialtyMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing Faculty.
 */
@Service
@Transactional
public class FacultyService {

    private final Logger log = LoggerFactory.getLogger(FacultyService.class);

    private FacultyRepository facultyRepository;

    private FacultyMapper facultyMapper;

    private SpecialtyMapper specialtyMapper;

    public FacultyService(FacultyRepository facultyRepository, FacultyMapper facultyMapper,
                          SpecialtyMapper specialtyMapper) {
        this.facultyRepository = facultyRepository;
        this.facultyMapper = facultyMapper;
        this.specialtyMapper = specialtyMapper;
    }

    /**
     * Save a faculty.
     *
     * @param facultyDTO the entity to save
     * @return the persisted entity
     */
    public FacultyDTO save(FacultyDTO facultyDTO) {
        log.debug("Request to save Faculty : {}", facultyDTO);

        Faculty faculty = facultyMapper.toEntity(facultyDTO);
        faculty = facultyRepository.save(faculty);
        return facultyMapper.toDto(faculty);
    }

    /**
     * Get all the faculties.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<FacultyDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Faculties");
        return facultyRepository.findAll(pageable)
            .map(facultyMapper::toDto);
    }


    /**
     * Get one faculty by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<FacultyDTO> findOne(Long id) {
        log.debug("Request to get Faculty : {}", id);
        return facultyRepository.findById(id)
            .map(facultyMapper::toDto);
    }

    /**
     * Delete the faculty by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Faculty : {}", id);
        facultyRepository.deleteById(id);
    }

    /**
     * Get specialties by faculty id.
     *
     * @param id the id of the entity
     * @return the list of specialties
     */
    @Transactional(readOnly = true)
    public Optional<List<SpecialtyDTO>> findSpecialtiesByFacultyId(Long id) {
        log.debug("Request to get Specialties of Faculty : {}", id);
        return facultyRepository.findById(id).map(Faculty::getSpecialties).map(ArrayList::new)
            .map(specialtyMapper::toDto);
    }
}
