package com.dyndyn.eventmanager.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the Student entity. This class is used in StudentResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /students?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class StudentCriteria implements Serializable {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LocalDateFilter birthday;

    private StringFilter telephone;

    private StringFilter address;

    private LongFilter eventStudentId;

    private LongFilter advertisementId;

    private LongFilter groupId;

    public StudentCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LocalDateFilter getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDateFilter birthday) {
        this.birthday = birthday;
    }

    public StringFilter getTelephone() {
        return telephone;
    }

    public void setTelephone(StringFilter telephone) {
        this.telephone = telephone;
    }

    public StringFilter getAddress() {
        return address;
    }

    public void setAddress(StringFilter address) {
        this.address = address;
    }

    public LongFilter getEventStudentId() {
        return eventStudentId;
    }

    public void setEventStudentId(LongFilter eventStudentId) {
        this.eventStudentId = eventStudentId;
    }

    public LongFilter getAdvertisementId() {
        return advertisementId;
    }

    public void setAdvertisementId(LongFilter advertisementId) {
        this.advertisementId = advertisementId;
    }

    public LongFilter getGroupId() {
        return groupId;
    }

    public void setGroupId(LongFilter groupId) {
        this.groupId = groupId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final StudentCriteria that = (StudentCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(birthday, that.birthday) &&
            Objects.equals(telephone, that.telephone) &&
            Objects.equals(address, that.address) &&
            Objects.equals(eventStudentId, that.eventStudentId) &&
            Objects.equals(advertisementId, that.advertisementId) &&
            Objects.equals(groupId, that.groupId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        birthday,
        telephone,
        address,
        eventStudentId,
        advertisementId,
        groupId
        );
    }

    @Override
    public String toString() {
        return "StudentCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (birthday != null ? "birthday=" + birthday + ", " : "") +
                (telephone != null ? "telephone=" + telephone + ", " : "") +
                (address != null ? "address=" + address + ", " : "") +
                (eventStudentId != null ? "eventStudentId=" + eventStudentId + ", " : "") +
                (advertisementId != null ? "advertisementId=" + advertisementId + ", " : "") +
                (groupId != null ? "groupId=" + groupId + ", " : "") +
            "}";
    }

}
