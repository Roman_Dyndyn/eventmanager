package com.dyndyn.eventmanager.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.ZonedDateTimeFilter;

/**
 * Criteria class for the Event entity. This class is used in EventResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /events?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class EventCriteria implements Serializable {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter title;

    private ZonedDateTimeFilter start;

    private ZonedDateTimeFilter end;

    private LongFilter teacherId;

    private LongFilter eventStudentId;

    private LongFilter studentId;

    public EventCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getTitle() {
        return title;
    }

    public void setTitle(StringFilter title) {
        this.title = title;
    }

    public ZonedDateTimeFilter getStart() {
        return start;
    }

    public void setStart(ZonedDateTimeFilter start) {
        this.start = start;
    }

    public ZonedDateTimeFilter getEnd() {
        return end;
    }

    public void setEnd(ZonedDateTimeFilter end) {
        this.end = end;
    }

    public LongFilter getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(LongFilter teacherId) {
        this.teacherId = teacherId;
    }

    public LongFilter getEventStudentId() {
        return eventStudentId;
    }

    public void setEventStudentId(LongFilter eventStudentId) {
        this.eventStudentId = eventStudentId;
    }

    public LongFilter getStudentId() {
        return studentId;
    }

    public void setStudentId(LongFilter studentId) {
        this.studentId = studentId;
    }

    public void setStudentId(Long studentId) {
        LongFilter longFilter = new LongFilter();
        longFilter.setEquals(studentId);
        this.studentId = longFilter;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final EventCriteria that = (EventCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(title, that.title) &&
            Objects.equals(start, that.start) &&
            Objects.equals(end, that.end) &&
            Objects.equals(teacherId, that.teacherId) &&
            Objects.equals(eventStudentId, that.eventStudentId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        title,
        start,
        end,
        teacherId,
        eventStudentId
        );
    }

    @Override
    public String toString() {
        return "EventCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (title != null ? "title=" + title + ", " : "") +
                (start != null ? "start=" + start + ", " : "") +
                (end != null ? "end=" + end + ", " : "") +
                (teacherId != null ? "teacherId=" + teacherId + ", " : "") +
                (eventStudentId != null ? "eventStudentId=" + eventStudentId + ", " : "") +
            "}";
    }

}
