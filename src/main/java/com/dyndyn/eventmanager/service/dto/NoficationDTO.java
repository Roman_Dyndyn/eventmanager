package com.dyndyn.eventmanager.service.dto;

import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;
import java.util.Objects;

public class NoficationDTO {

    @NotNull
    private String title;

    @NotNull
    private ZonedDateTime start;

    public NoficationDTO(@NotNull String title, @NotNull ZonedDateTime start) {
        this.title = title;
        this.start = start;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ZonedDateTime getStart() {
        return start;
    }

    public void setStart(ZonedDateTime start) {
        this.start = start;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NoficationDTO that = (NoficationDTO) o;
        return Objects.equals(title, that.title) &&
            Objects.equals(start, that.start);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, start);
    }

    @Override
    public String toString() {
        return "NoficationDTO{" +
            "title='" + title + '\'' +
            ", start=" + start +
            '}';
    }
}
