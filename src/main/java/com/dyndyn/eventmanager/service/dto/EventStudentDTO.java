package com.dyndyn.eventmanager.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the EventStudent entity.
 */
public class EventStudentDTO implements Serializable {

    private Long id;

    @NotNull
    private Integer minutes;

    private String googleEventId;

    private String studentName;

    private Long studentId;

    private String eventTitle;

    private Long eventId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getMinutes() {
        return minutes;
    }

    public void setMinutes(Integer minutes) {
        this.minutes = minutes;
    }

    public String getGoogleEventId() {
        return googleEventId;
    }

    public void setGoogleEventId(String googleEventId) {
        this.googleEventId = googleEventId;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public String getEventTitle() {
        return eventTitle;
    }

    public void setEventTitle(String eventTitle) {
        this.eventTitle = eventTitle;
    }

    public Long getEventId() {
        return eventId;
    }

    public void setEventId(Long eventId) {
        this.eventId = eventId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        EventStudentDTO eventStudentDTO = (EventStudentDTO) o;
        if (eventStudentDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), eventStudentDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "EventStudentDTO{" +
            "id=" + getId() +
            ", minutes=" + getMinutes() +
            ", googleEventId='" + getGoogleEventId() + "'" +
            ", student=" + getStudentId() +
            ", event=" + getEventId() +
            "}";
    }
}
