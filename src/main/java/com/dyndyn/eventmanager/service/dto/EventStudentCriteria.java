package com.dyndyn.eventmanager.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the EventStudent entity. This class is used in EventStudentResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /event-students?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class EventStudentCriteria implements Serializable {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private IntegerFilter minutes;

    private StringFilter googleEventId;

    private LongFilter studentId;

    private LongFilter eventId;

    public EventStudentCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getMinutes() {
        return minutes;
    }

    public void setMinutes(IntegerFilter minutes) {
        this.minutes = minutes;
    }

    public StringFilter getGoogleEventId() {
        return googleEventId;
    }

    public void setGoogleEventId(StringFilter googleEventId) {
        this.googleEventId = googleEventId;
    }

    public LongFilter getStudentId() {
        return studentId;
    }

    public void setStudentId(LongFilter studentId) {
        this.studentId = studentId;
    }

    public void setStudentId(Long studentId) {
        LongFilter longFilter = new LongFilter();
        longFilter.setEquals(studentId);
        this.studentId = longFilter;
    }

    public LongFilter getEventId() {
        return eventId;
    }

    public void setEventId(LongFilter eventId) {
        this.eventId = eventId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final EventStudentCriteria that = (EventStudentCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(minutes, that.minutes) &&
            Objects.equals(googleEventId, that.googleEventId) &&
            Objects.equals(studentId, that.studentId) &&
            Objects.equals(eventId, that.eventId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        minutes,
        googleEventId,
        studentId,
        eventId
        );
    }

    @Override
    public String toString() {
        return "EventStudentCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (minutes != null ? "minutes=" + minutes + ", " : "") +
                (googleEventId != null ? "googleEventId=" + googleEventId + ", " : "") +
                (studentId != null ? "studentId=" + studentId + ", " : "") +
                (eventId != null ? "eventId=" + eventId + ", " : "") +
            "}";
    }

}
