package com.dyndyn.eventmanager.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the Teacher entity. This class is used in TeacherResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /teachers?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class TeacherCriteria implements Serializable {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LocalDateFilter birthday;

    private StringFilter telephone;

    private StringFilter address;

    private LongFilter eventId;

    private LongFilter advertisementId;

    public TeacherCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LocalDateFilter getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDateFilter birthday) {
        this.birthday = birthday;
    }

    public StringFilter getTelephone() {
        return telephone;
    }

    public void setTelephone(StringFilter telephone) {
        this.telephone = telephone;
    }

    public StringFilter getAddress() {
        return address;
    }

    public void setAddress(StringFilter address) {
        this.address = address;
    }

    public LongFilter getEventId() {
        return eventId;
    }

    public void setEventId(LongFilter eventId) {
        this.eventId = eventId;
    }

    public LongFilter getAdvertisementId() {
        return advertisementId;
    }

    public void setAdvertisementId(LongFilter advertisementId) {
        this.advertisementId = advertisementId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final TeacherCriteria that = (TeacherCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(birthday, that.birthday) &&
            Objects.equals(telephone, that.telephone) &&
            Objects.equals(address, that.address) &&
            Objects.equals(eventId, that.eventId) &&
            Objects.equals(advertisementId, that.advertisementId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        birthday,
        telephone,
        address,
        eventId,
        advertisementId
        );
    }

    @Override
    public String toString() {
        return "TeacherCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (birthday != null ? "birthday=" + birthday + ", " : "") +
                (telephone != null ? "telephone=" + telephone + ", " : "") +
                (address != null ? "address=" + address + ", " : "") +
                (eventId != null ? "eventId=" + eventId + ", " : "") +
                (advertisementId != null ? "advertisementId=" + advertisementId + ", " : "") +
            "}";
    }

}
