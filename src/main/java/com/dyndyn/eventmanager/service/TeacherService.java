package com.dyndyn.eventmanager.service;

import com.dyndyn.eventmanager.domain.Teacher;
import com.dyndyn.eventmanager.domain.User;
import com.dyndyn.eventmanager.repository.TeacherRepository;
import com.dyndyn.eventmanager.repository.UserRepository;
import com.dyndyn.eventmanager.service.dto.TeacherDTO;
import com.dyndyn.eventmanager.service.mapper.TeacherMapper;
import com.dyndyn.eventmanager.web.rest.errors.EmailAlreadyUsedException;
import com.dyndyn.eventmanager.web.rest.errors.InternalServerErrorException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Teacher.
 */
@Service
@Transactional
public class TeacherService {

    private final Logger log = LoggerFactory.getLogger(TeacherService.class);

    private static final String DEFAULT_PASSWORD = "password";

    private TeacherRepository teacherRepository;

    private UserRepository userRepository;

    private PasswordEncoder passwordEncoder;

    private TeacherMapper teacherMapper;

    private UserService userService;

    public TeacherService(TeacherRepository teacherRepository, UserRepository userRepository,
                          PasswordEncoder passwordEncoder, TeacherMapper teacherMapper, UserService userService) {
        this.teacherRepository = teacherRepository;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.teacherMapper = teacherMapper;
        this.userService = userService;
    }

    /**
     * Save a teacher.
     *
     * @param teacherDTO the entity to save
     * @return the persisted entity
     */
    public TeacherDTO save(TeacherDTO teacherDTO) {
        log.debug("Request to save Teacher : {}", teacherDTO);

        Teacher teacher = teacherMapper.toEntity(teacherDTO);
        teacher.setPassword(passwordEncoder.encode(DEFAULT_PASSWORD));
        teacher = teacherRepository.save(teacher);
        return teacherMapper.toDto(teacher);
    }

    /**
     * update a teacher.
     *
     * @param teacherDTO the entity to update
     * @return the persisted entity
     */
    public TeacherDTO update(TeacherDTO teacherDTO) {
        log.debug("Request to save Teacher : {}", teacherDTO);

        Optional<User> existingUser = userRepository.findOneByEmailIgnoreCase(teacherDTO.getEmail());
        if (existingUser.isPresent() && (!existingUser.get().getLogin().equalsIgnoreCase(teacherDTO.getLogin()))) {
            throw new EmailAlreadyUsedException();
        }

        Teacher teacher = teacherRepository.findOneByLogin(teacherDTO.getLogin()).orElseThrow(() -> new InternalServerErrorException("Current user login not found"));
        teacher.setFirstName(teacherDTO.getFirstName());
        teacher.setLastName(teacherDTO.getLastName());
        teacher.setEmail(teacherDTO.getEmail().toLowerCase());
        teacher.setLangKey(teacherDTO.getLangKey());
        teacher.setImageUrl(teacherDTO.getImageUrl());
        teacher.setBirthday(teacherDTO.getBirthday());
        teacher.setTelephone(teacherDTO.getTelephone());
        teacher.setAddress(teacherDTO.getAddress());
        userService.clearUserCaches(teacher);
        log.debug("Changed Information for User: {}", teacher);

        return teacherMapper.toDto(teacherRepository.save(teacher));
    }

    /**
     * Get all the teachers.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<TeacherDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Teachers");
        return teacherRepository.findAll(pageable)
            .map(teacherMapper::toDto);
    }


    /**
     * Get one teacher by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<TeacherDTO> findOne(Long id) {
        log.debug("Request to get Teacher : {}", id);
        return teacherRepository.findById(id)
            .map(teacherMapper::toDto);
    }

    /**
     * Delete the teacher by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Teacher : {}", id);
        teacherRepository.deleteById(id);
    }
}
