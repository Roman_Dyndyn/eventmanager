package com.dyndyn.eventmanager.service;

import com.dyndyn.eventmanager.domain.Student;
import com.dyndyn.eventmanager.domain.User;
import com.dyndyn.eventmanager.repository.StudentRepository;
import com.dyndyn.eventmanager.repository.UserRepository;
import com.dyndyn.eventmanager.service.dto.StudentDTO;
import com.dyndyn.eventmanager.service.mapper.GroupMapper;
import com.dyndyn.eventmanager.service.mapper.StudentMapper;
import com.dyndyn.eventmanager.web.rest.errors.EmailAlreadyUsedException;
import com.dyndyn.eventmanager.web.rest.errors.InternalServerErrorException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Student.
 */
@Service
@Transactional
public class StudentService {

    private final Logger log = LoggerFactory.getLogger(StudentService.class);

    private static final String DEFAULT_PASSWORD = "password";

    private StudentRepository studentRepository;

    private UserRepository userRepository;

    private PasswordEncoder passwordEncoder;

    private StudentMapper studentMapper;

    private GroupMapper groupMapper;

    private UserService userService;

    public StudentService(StudentRepository studentRepository, UserRepository userRepository, PasswordEncoder passwordEncoder,
                          StudentMapper studentMapper, GroupMapper groupMapper, UserService userService) {
        this.studentRepository = studentRepository;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.studentMapper = studentMapper;
        this.groupMapper = groupMapper;
        this.userService = userService;
    }

    /**
     * Save a student.
     *
     * @param studentDTO the entity to save
     * @return the persisted entity
     */
    public StudentDTO save(StudentDTO studentDTO) {
        log.debug("Request to save Student : {}", studentDTO);

        Student student = studentMapper.toEntity(studentDTO);
        student.setPassword(passwordEncoder.encode(DEFAULT_PASSWORD));
        student = studentRepository.save(student);
        return studentMapper.toDto(student);
    }

    /**
     * update a student.
     *
     * @param studentDTO the entity to save
     * @return the persisted entity
     */
    public StudentDTO update(StudentDTO studentDTO) {
        log.debug("Request to update Student : {}", studentDTO);
        Optional<User> existingUser = userRepository.findOneByEmailIgnoreCase(studentDTO.getEmail());
        if (existingUser.isPresent() && (!existingUser.get().getLogin().equalsIgnoreCase(studentDTO.getLogin()))) {
            throw new EmailAlreadyUsedException();
        }

        Student student = studentRepository.findOneByLogin(studentDTO.getLogin()).orElseThrow(() -> new InternalServerErrorException("Current user login not found"));
        student.setFirstName(studentDTO.getFirstName());
        student.setLastName(studentDTO.getLastName());
        student.setEmail(studentDTO.getEmail().toLowerCase());
        student.setLangKey(studentDTO.getLangKey());
        student.setImageUrl(studentDTO.getImageUrl());
        student.setBirthday(studentDTO.getBirthday());
        student.setTelephone(studentDTO.getTelephone());
        student.setAddress(studentDTO.getAddress());
        student.setGroup(groupMapper.fromId(studentDTO.getGroupId()));
        userService.clearUserCaches(student);
        log.debug("Changed Information for User: {}", student);

        return studentMapper.toDto(studentRepository.save(student));
    }

    /**
     * Get all the students.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<StudentDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Students");
        return studentRepository.findAll(pageable)
            .map(studentMapper::toDto);
    }


    /**
     * Get one student by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<StudentDTO> findOne(Long id) {
        log.debug("Request to get Student : {}", id);
        return studentRepository.findById(id)
            .map(studentMapper::toDto);
    }

    /**
     * Delete the student by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Student : {}", id);
        studentRepository.deleteById(id);
    }
}
