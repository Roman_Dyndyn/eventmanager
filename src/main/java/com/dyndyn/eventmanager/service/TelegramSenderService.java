package com.dyndyn.eventmanager.service;

import com.dyndyn.eventmanager.config.ApplicationProperties;
import com.dyndyn.eventmanager.service.dto.NoficationDTO;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.bots.DefaultBotOptions;
import org.telegram.telegrambots.meta.ApiContext;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.time.format.DateTimeFormatter;

@Service
@Profile("!test")
public class TelegramSenderService extends DefaultAbsSender {

    private ApplicationProperties applicationProperties;

    public TelegramSenderService(ApplicationProperties applicationProperties) {
        super(ApiContext.getInstance(DefaultBotOptions.class));
        this.applicationProperties = applicationProperties;
    }

    public Integer sendTextMessage(Long chatId, String text) throws TelegramApiException {
        SendMessage message = new SendMessage()
            .setChatId(chatId)
            .setText(text);
        return execute(message).getMessageId();
    }

    @Override
    public String getBotToken() {
        return applicationProperties.getTelegram().getToken();
    }
}
