package com.dyndyn.eventmanager.service;

import com.dyndyn.eventmanager.domain.EventStudent;
import com.dyndyn.eventmanager.domain.User;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventDateTime;
import com.google.api.services.calendar.model.EventReminder;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collections;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

@Service
public class GoogleCalendarService {

    private final Logger log = LoggerFactory.getLogger(GoogleCalendarService.class);
    private final String calendarId = "primary";
    private final String APPLICATION_NAME = "Event-Manager";
    private final UserService userService;

    public GoogleCalendarService(UserService userService) {
        this.userService = userService;
    }

    public void addEventToUsersCalendar(EventStudent eventStudent) {
        if (StringUtils.isEmpty(eventStudent.getStudent().getAccessToken())) {
            return;
        }

        Event result = insertEventToUserCalendar(convert(eventStudent), eventStudent.getStudent());
        eventStudent.setGoogleEventId(result.getId());
        log.info("GoogleEventId has been added to EventStudent {}", eventStudent);
    }

    public void updateEventInUsersCalendar(EventStudent eventStudent) {
        if (StringUtils.isEmpty(eventStudent.getStudent().getAccessToken())) {
            return;
        }

        Event event = convert(eventStudent);
        final NetHttpTransport HTTP_TRANSPORT;
        try {
            HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();

            Calendar service = new Calendar.Builder(HTTP_TRANSPORT, new JacksonFactory(), new GoogleCredential()
                .setAccessToken(eventStudent.getStudent().getAccessToken()))
                .setApplicationName(APPLICATION_NAME)
                .build();

            event = service.events().update(calendarId, eventStudent.getGoogleEventId(), event).execute();
            log.info("Event has been inserted for user {}: {}", eventStudent.getStudent().getLogin(), event);
        } catch (GoogleJsonResponseException e) {
            if (e.getStatusCode() == HttpStatus.UNAUTHORIZED.value()) {
                insertEventToUserCalendar(event, userService.refreshAccessToken(eventStudent.getStudent()));
            } else {
                log.error("Can't insert google event", e);
            }
        } catch (GeneralSecurityException | IOException e) {
            log.error("Can't insert google event", e);
        }
        log.info("GoogleEvent has been updated {}", eventStudent);
    }


    public void removeEventFromUserCalendar(EventStudent eventStudent) {
        final NetHttpTransport HTTP_TRANSPORT;
        try {
            HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();

            Calendar service = new Calendar.Builder(HTTP_TRANSPORT, new JacksonFactory(), new GoogleCredential()
                .setAccessToken(eventStudent.getStudent().getAccessToken()))
                .setApplicationName(APPLICATION_NAME)
                .build();

            service.events().delete(calendarId, eventStudent.getGoogleEventId()).execute();
            log.info("Event has been deleted for user {}: {}", eventStudent.getStudent().getLogin(), eventStudent);
        } catch (GoogleJsonResponseException e) {
            if (e.getStatusCode() == HttpStatus.UNAUTHORIZED.value()) {
                userService.refreshAccessToken(eventStudent.getStudent());
                removeEventFromUserCalendar(eventStudent);
            } else {
                log.error("Can't delete google event", e);
            }
        } catch (GeneralSecurityException | IOException e) {
            log.error("Can't delete google event", e);
        }
    }

    private Event insertEventToUserCalendar(Event event, User user) {
        final NetHttpTransport HTTP_TRANSPORT;
        try {
            HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();

            Calendar service = new Calendar.Builder(HTTP_TRANSPORT, new JacksonFactory(), new GoogleCredential()
                .setAccessToken(user.getAccessToken()))
                .setApplicationName(APPLICATION_NAME)
                .build();

            event = service.events().insert(calendarId, event).execute();
            log.info("Event has been inserted for user {}: {}", user.getLogin(), event);
        } catch (GoogleJsonResponseException e) {
            if (e.getStatusCode() == HttpStatus.UNAUTHORIZED.value()) {
                insertEventToUserCalendar(event, userService.refreshAccessToken(user));
            } else {
                log.error("Can't insert google event", e);
            }
        } catch (GeneralSecurityException | IOException e) {
            log.error("Can't insert google event", e);
        }
        return event;
    }

    private com.google.api.services.calendar.model.Event convert(EventStudent eventStudent) {
        Event event = new com.google.api.services.calendar.model.Event()
            .setSummary(eventStudent.getEvent().getTitle())
            .setLocation("Ivano-Frankivsk")
            .setDescription(eventStudent.getEvent().getDescription());

        long startMillis = TimeUnit.SECONDS.toMillis(eventStudent.getEvent().getStart().toSecondOfDay()) +
            TimeUnit.DAYS.toMillis(eventStudent.getEvent().getMetadata().getStartDate().toEpochDay());
        EventDateTime start = new EventDateTime()
            .setDateTime(new DateTime(startMillis - TimeZone.getDefault().getOffset(startMillis)))
            .setTimeZone(TimeZone.getDefault().getID());
        event.setStart(start);

        long endMillis = TimeUnit.SECONDS.toMillis(eventStudent.getEvent().getEnd().toSecondOfDay()) +
            TimeUnit.DAYS.toMillis(eventStudent.getEvent().getMetadata().getStartDate().toEpochDay());
        EventDateTime end = new EventDateTime()
            .setDateTime(new DateTime(endMillis - TimeZone.getDefault().getOffset(endMillis)))
            .setTimeZone(TimeZone.getDefault().getID());
        event.setEnd(end);

        if (eventStudent.getEvent().getMetadata().getEndDate() != null) {
            event.setRecurrence(Collections.singletonList(String.format("RRULE:FREQ=DAILY;INTERVAL=%d;UNTIL=%s;",
                eventStudent.getEvent().getMetadata().getInterval(),
                ZonedDateTime.of(eventStudent.getEvent().getMetadata().getEndDate(),
                    eventStudent.getEvent().getEnd(), ZoneId.systemDefault()).format(DateTimeFormatter.
                    ofPattern("yyyyMMdd'T'HHmmss'Z'")))));
        }

        EventReminder[] reminderOverrides = new EventReminder[]{
            new EventReminder().setMethod("email").setMinutes(24 * 60),
            new EventReminder().setMethod("popup").setMinutes(10),
        };
        Event.Reminders reminders = new Event.Reminders()
            .setUseDefault(false)
            .setOverrides(Arrays.asList(reminderOverrides));
        event.setReminders(reminders);
        return event;
    }

}
