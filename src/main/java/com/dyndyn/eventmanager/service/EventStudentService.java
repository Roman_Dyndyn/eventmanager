package com.dyndyn.eventmanager.service;

import com.dyndyn.eventmanager.domain.*;
import com.dyndyn.eventmanager.repository.EventStudentRepository;
import com.dyndyn.eventmanager.service.dto.EventStudentDTO;
import com.dyndyn.eventmanager.service.mapper.EventStudentMapper;
import com.dyndyn.eventmanager.service.util.UnitExpression;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing EventStudent.
 */
@Service
public class EventStudentService {

    private final Logger log = LoggerFactory.getLogger(EventStudentService.class);

    private EventStudentRepository eventStudentRepository;

    private EventStudentMapper eventStudentMapper;

    public EventStudentService(EventStudentRepository eventStudentRepository, EventStudentMapper eventStudentMapper) {
        this.eventStudentRepository = eventStudentRepository;
        this.eventStudentMapper = eventStudentMapper;
    }

    /**
     * Save a eventStudent.
     *
     * @param eventStudentDTO the entity to save
     * @return the persisted entity
     */
    @Transactional
    public EventStudentDTO save(EventStudentDTO eventStudentDTO) {
        log.debug("Request to save EventStudent : {}", eventStudentDTO);

        EventStudent eventStudent = eventStudentMapper.toEntity(eventStudentDTO);
        eventStudent = eventStudentRepository.save(eventStudent);
        return eventStudentMapper.toDto(eventStudent);
    }

    /**
     * Get all the eventStudents.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<EventStudentDTO> findAll(Pageable pageable) {
        log.debug("Request to get all EventStudents");
        return eventStudentRepository.findAll(pageable)
            .map(eventStudentMapper::toDto);
    }

    /**
     * Get one eventStudent by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<EventStudentDTO> findOne(Long id) {
        log.debug("Request to get EventStudent : {}", id);
        return eventStudentRepository.findById(id)
            .map(eventStudentMapper::toDto);
    }

    /**
     * Delete the eventStudent by id.
     *
     * @param id the id of the entity
     */
    @Transactional
    public void delete(Long id) {
        log.debug("Request to delete EventStudent : {}", id);
        eventStudentRepository.deleteById(id);
    }

    /**
     * SELECT *
     * FROM event_student es
     *   INNER JOIN event ev on es.event_id=ev.id
     *   INNER JOIN event_metadata em on ev.id = em.event_id
     * WHERE (TIMESTAMPDIFF(MINUTE, em.repeat_start, current_timestamp) - TIME_TO_SEC( ev.start) / 60 + es.minutes + 5) % (em.`interval` * 24 * 60)
     * BETWEEN 0 AND 5 AND em.repeat_end >= CURRENT_TIMESTAMP;
     * @return List of students to notify
     */
    @Transactional(readOnly = true)
    public List<EventStudent> getAllStudentsToNotify() {
        return eventStudentRepository.findAll((root, query, cb) -> {

            Join<EventStudent, Event> eventStudentJoinEvent = root.join(EventStudent_.event, JoinType.INNER);
            Join<Event, EventMetadata> eventJoinEventMetadata = eventStudentJoinEvent.join(Event_.metadata, JoinType.INNER);
            Expression<Integer> currentMinusStartDate = cb.function(
                "TIMESTAMPDIFF",
                Integer.class,
                new UnitExpression(null, String.class, "MINUTE"),
                eventJoinEventMetadata.get(EventMetadata_.startDate),
                cb.currentTimestamp());

            Expression<Number> timeToMin = cb.quot(cb.function(
                "TIME_TO_SEC",
                Integer.class,
                eventStudentJoinEvent.get(Event_.start)
            ), 60);
            Expression<Integer> calculatedMinutes = cb.sum(cb.diff(cb.sum(currentMinusStartDate, root.get(EventStudent_.minutes)),
                timeToMin), 5).as(Integer.class);

            Expression<Integer> mod = cb.mod(calculatedMinutes, cb.prod(eventJoinEventMetadata.get(EventMetadata_.interval), 24 * 60));
            return cb.and(cb.between(mod, 0, 5),
                cb.or(cb.greaterThanOrEqualTo(eventJoinEventMetadata.get(EventMetadata_.endDate).as(Date.class),
                    cb.currentDate()),
                    cb.and(
                        cb.isNull(eventJoinEventMetadata.get(EventMetadata_.endDate)),
                        cb.equal(eventJoinEventMetadata.get(EventMetadata_.startDate), cb.currentDate())
                        )));
        });
    }
}
