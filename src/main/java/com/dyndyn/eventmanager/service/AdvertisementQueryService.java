package com.dyndyn.eventmanager.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.dyndyn.eventmanager.domain.Advertisement;
import com.dyndyn.eventmanager.domain.*; // for static metamodels
import com.dyndyn.eventmanager.repository.AdvertisementRepository;
import com.dyndyn.eventmanager.service.dto.AdvertisementCriteria;
import com.dyndyn.eventmanager.service.dto.AdvertisementDTO;
import com.dyndyn.eventmanager.service.mapper.AdvertisementMapper;

/**
 * Service for executing complex queries for Advertisement entities in the database.
 * The main input is a {@link AdvertisementCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link AdvertisementDTO} or a {@link Page} of {@link AdvertisementDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class AdvertisementQueryService extends QueryService<Advertisement> {

    private final Logger log = LoggerFactory.getLogger(AdvertisementQueryService.class);

    private AdvertisementRepository advertisementRepository;

    private AdvertisementMapper advertisementMapper;

    public AdvertisementQueryService(AdvertisementRepository advertisementRepository, AdvertisementMapper advertisementMapper) {
        this.advertisementRepository = advertisementRepository;
        this.advertisementMapper = advertisementMapper;
    }

    /**
     * Return a {@link List} of {@link AdvertisementDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<AdvertisementDTO> findByCriteria(AdvertisementCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Advertisement> specification = createSpecification(criteria);
        return advertisementMapper.toDto(advertisementRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link AdvertisementDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<AdvertisementDTO> findByCriteria(AdvertisementCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Advertisement> specification = createSpecification(criteria);
        return advertisementRepository.findAll(specification, page)
            .map(advertisementMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(AdvertisementCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Advertisement> specification = createSpecification(criteria);
        return advertisementRepository.count(specification);
    }

    /**
     * Function to convert AdvertisementCriteria to a {@link Specification}
     */
    private Specification<Advertisement> createSpecification(AdvertisementCriteria criteria) {
        Specification<Advertisement> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Advertisement_.id));
            }
            if (criteria.getTitle() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTitle(), Advertisement_.title));
            }
            if (criteria.getTeacherId() != null) {
                specification = specification.and(buildSpecification(criteria.getTeacherId(),
                    root -> root.join(Advertisement_.teacher, JoinType.LEFT).get(Teacher_.id)));
            }
            if (criteria.getStudentId() != null) {
                specification = specification.and(buildSpecification(criteria.getStudentId(),
                    root -> root.join(Advertisement_.students, JoinType.LEFT).get(User_.id)));
            }
        }
        return specification;
    }
}
