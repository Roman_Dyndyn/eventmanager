package com.dyndyn.eventmanager.service;

import com.dyndyn.eventmanager.domain.Specialty;
import com.dyndyn.eventmanager.repository.SpecialtyRepository;
import com.dyndyn.eventmanager.service.dto.GroupDTO;
import com.dyndyn.eventmanager.service.dto.SpecialtyDTO;
import com.dyndyn.eventmanager.service.mapper.GroupMapper;
import com.dyndyn.eventmanager.service.mapper.SpecialtyMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing Specialty.
 */
@Service
@Transactional
public class SpecialtyService {

    private final Logger log = LoggerFactory.getLogger(SpecialtyService.class);

    private SpecialtyRepository specialtyRepository;

    private SpecialtyMapper specialtyMapper;

    private GroupMapper groupMapper;

    public SpecialtyService(SpecialtyRepository specialtyRepository, SpecialtyMapper specialtyMapper,
                            GroupMapper groupMapper) {
        this.specialtyRepository = specialtyRepository;
        this.specialtyMapper = specialtyMapper;
        this.groupMapper = groupMapper;
    }

    /**
     * Save a specialty.
     *
     * @param specialtyDTO the entity to save
     * @return the persisted entity
     */
    public SpecialtyDTO save(SpecialtyDTO specialtyDTO) {
        log.debug("Request to save Specialty : {}", specialtyDTO);

        Specialty specialty = specialtyMapper.toEntity(specialtyDTO);
        specialty = specialtyRepository.save(specialty);
        return specialtyMapper.toDto(specialty);
    }

    /**
     * Get all the specialties.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SpecialtyDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Specialties");
        return specialtyRepository.findAll(pageable)
            .map(specialtyMapper::toDto);
    }


    /**
     * Get one specialty by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<SpecialtyDTO> findOne(Long id) {
        log.debug("Request to get Specialty : {}", id);
        return specialtyRepository.findById(id)
            .map(specialtyMapper::toDto);
    }

    /**
     * Delete the specialty by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Specialty : {}", id);
        specialtyRepository.deleteById(id);
    }

    /**
     * Get groups by specialty id.
     *
     * @param id the id of the entity
     * @return the list of groups
     */
    @Transactional(readOnly = true)
    public Optional<List<GroupDTO>> findGroupsBySpecialtyId(Long id) {
        log.debug("Request to get Groups of Specialty : {}", id);
        return specialtyRepository.findById(id).map(Specialty::getGroups).map(ArrayList::new).map(groupMapper::toDto);
    }
}
