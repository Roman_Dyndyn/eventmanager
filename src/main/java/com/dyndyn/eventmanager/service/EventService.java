package com.dyndyn.eventmanager.service;

import com.dyndyn.eventmanager.domain.*;
import com.dyndyn.eventmanager.repository.EventRepository;
import com.dyndyn.eventmanager.service.dto.EventDTO;
import com.dyndyn.eventmanager.service.mapper.EventMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Event.
 */
@Service
@Transactional
public class EventService {

    private final Logger log = LoggerFactory.getLogger(EventService.class);

    private final Integer minutes = 60;

    private EventRepository eventRepository;

    private EventMapper eventMapper;

    public EventService(EventRepository eventRepository, EventMapper eventMapper) {
        this.eventRepository = eventRepository;
        this.eventMapper = eventMapper;
    }

    /**
     * Save a event.
     *
     * @param eventDTO the entity to save
     * @return the persisted entity
     */
    public EventDTO save(EventDTO eventDTO) {
        log.debug("Request to save Event : {}", eventDTO);
        Set<EventStudent> eventStudents = new HashSet<>();
        if (eventDTO.getId() != null) {
            eventRepository.findById(eventDTO.getId()).map(Event::getEventStudents).ifPresent(existingEventStudents -> {
                Set<Long> existingStudentIds = existingEventStudents.stream().map(eventStudent -> eventStudent.getStudent().getId()).collect(Collectors.toSet());

                Set<Long> needToDeleteIds = new HashSet<>(existingStudentIds);
                needToDeleteIds.removeAll(eventDTO.getStudentIds());

                eventDTO.getStudentIds().removeAll(existingStudentIds);

                existingEventStudents.removeIf(eventStudent -> needToDeleteIds.contains(eventStudent.getStudent().getId()));
                eventStudents.addAll(existingEventStudents);
            });
        }

        Event event = eventMapper.toEntity(eventDTO);
        event.getEventStudents().forEach(eventStudent -> eventStudent.setMinutes(minutes));
        event.getEventStudents().addAll(eventStudents);
        event = eventRepository.save(event);
        return eventMapper.toDto(event);
    }

    /**
     * Get all the events.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<EventDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Events");
        return eventRepository.findAll(pageable)
            .map(eventMapper::toDto);
    }


    /**
     * Get one event by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<EventDTO> findOne(Long id) {
        log.debug("Request to get Event : {}", id);
        return eventRepository.findById(id)
            .map(eventMapper::toDto);
    }

    /**
     * Delete the event by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Event : {}", id);
        eventRepository.deleteById(id);
    }

    /**
     * Get all the eventStudents for User.
     *
     * @param chatId User Telegram chat id
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<EventDTO> findAllByStudentChatId(Long chatId) {
        log.debug("Request to get all EventStudents for User with chat id {}", chatId);
        return eventRepository.findAll((root, query, cb) -> {
            Join<Event, EventStudent> eventJoinEventStudent = root.join(Event_.eventStudents, JoinType.INNER);
            Join<EventStudent, Student> eventStudentJoinStudent = eventJoinEventStudent.join(EventStudent_.student, JoinType.INNER);
            Join<Event, EventMetadata> eventJoinEventMetadata = root.join(Event_.metadata, JoinType.INNER);

            Predicate datePredicate = cb.or(cb.greaterThanOrEqualTo(eventJoinEventMetadata.get(EventMetadata_.endDate).as(Date.class),
                cb.currentDate()),
                cb.and(
                    cb.isNull(eventJoinEventMetadata.get(EventMetadata_.endDate))),
                cb.greaterThanOrEqualTo(eventJoinEventMetadata.get(EventMetadata_.startDate).as(Date.class), cb.currentDate())
            );

            return cb.and(cb.equal(eventStudentJoinStudent.get(Student_.chatId), chatId), datePredicate);

        }).stream().map(eventMapper::toDto).collect(Collectors.toList());
    }
}
