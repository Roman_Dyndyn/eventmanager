package com.dyndyn.eventmanager.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.dyndyn.eventmanager.domain.EventStudent;
import com.dyndyn.eventmanager.domain.*; // for static metamodels
import com.dyndyn.eventmanager.repository.EventStudentRepository;
import com.dyndyn.eventmanager.service.dto.EventStudentCriteria;
import com.dyndyn.eventmanager.service.dto.EventStudentDTO;
import com.dyndyn.eventmanager.service.mapper.EventStudentMapper;

/**
 * Service for executing complex queries for EventStudent entities in the database.
 * The main input is a {@link EventStudentCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link EventStudentDTO} or a {@link Page} of {@link EventStudentDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class EventStudentQueryService extends QueryService<EventStudent> {

    private final Logger log = LoggerFactory.getLogger(EventStudentQueryService.class);

    private EventStudentRepository eventStudentRepository;

    private EventStudentMapper eventStudentMapper;

    public EventStudentQueryService(EventStudentRepository eventStudentRepository, EventStudentMapper eventStudentMapper) {
        this.eventStudentRepository = eventStudentRepository;
        this.eventStudentMapper = eventStudentMapper;
    }

    /**
     * Return a {@link List} of {@link EventStudentDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<EventStudentDTO> findByCriteria(EventStudentCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<EventStudent> specification = createSpecification(criteria);
        return eventStudentMapper.toDto(eventStudentRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link EventStudentDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<EventStudentDTO> findByCriteria(EventStudentCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<EventStudent> specification = createSpecification(criteria);
        return eventStudentRepository.findAll(specification, page)
            .map(eventStudentMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(EventStudentCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<EventStudent> specification = createSpecification(criteria);
        return eventStudentRepository.count(specification);
    }

    /**
     * Function to convert EventStudentCriteria to a {@link Specification}
     */
    private Specification<EventStudent> createSpecification(EventStudentCriteria criteria) {
        Specification<EventStudent> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), EventStudent_.id));
            }
            if (criteria.getMinutes() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getMinutes(), EventStudent_.minutes));
            }
            if (criteria.getGoogleEventId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getGoogleEventId(), EventStudent_.googleEventId));
            }
            if (criteria.getStudentId() != null) {
                specification = specification.and(buildSpecification(criteria.getStudentId(),
                    root -> root.join(EventStudent_.student, JoinType.LEFT).get(User_.id)));
            }
            if (criteria.getEventId() != null) {
                specification = specification.and(buildSpecification(criteria.getEventId(),
                    root -> root.join(EventStudent_.event, JoinType.LEFT).get(Event_.id)));
            }
        }
        return specification;
    }
}
