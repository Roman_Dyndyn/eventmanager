package com.dyndyn.eventmanager.service;

import com.dyndyn.eventmanager.domain.EventStudent;
import com.dyndyn.eventmanager.service.dto.NoficationDTO;
import com.dyndyn.eventmanager.web.websocket.MessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.messaging.simp.user.SimpUser;
import org.springframework.messaging.simp.user.SimpUserRegistry;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

/**
 * Service Implementation for Notification.
 */
@Service
@Profile("!test")
public class NotificationService {

    private final String NOTIFICATION = "Подія %s розпочнеться о %s";
    private final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm";

    private final Logger log = LoggerFactory.getLogger(NotificationService.class);

    private final static String topicUser = "/topic/messages-user";

    private EventStudentService eventStudentService;

    private SimpUserRegistry simpUserRegistry;

    private MessageService messageService;

    private MailService mailService;

    private TelegramSenderService telegramSenderService;

    private SmsSenderService smsSenderService;

    public NotificationService(EventStudentService eventStudentService, SimpUserRegistry simpUserRegistry,
                               MessageService messageService, MailService mailService,
                               TelegramSenderService telegramSenderService, SmsSenderService smsSenderService) {
        this.eventStudentService = eventStudentService;
        this.simpUserRegistry = simpUserRegistry;
        this.messageService = messageService;
        this.mailService = mailService;
        this.telegramSenderService = telegramSenderService;
        this.smsSenderService = smsSenderService;
    }

    @Scheduled(cron = "0 */5 * ? * *")
    public void notifyStudent() {
        List<EventStudent> eventStudents = eventStudentService.getAllStudentsToNotify();
        log.debug("Notifying {} EventStudent", eventStudents.size());

        Set<String> userNames = simpUserRegistry.getUsers().stream().map(SimpUser::getName).collect(Collectors.toSet());
        Map<Boolean, List<EventStudent>> map = eventStudents.stream()
            .collect(Collectors.partitioningBy(es -> es.getStudent().isTelegramEnabled() &&
                nonNull(es.getStudent().getChatId())));
        map.get(true).forEach(this::sendTelegramMessage);
        map.get(false).forEach(this::sendEmail);

        eventStudents.stream().filter(es -> userNames.contains(es.getStudent().getLogin()))
            .forEach(this::sendWS);
        eventStudents.stream().filter(es -> nonNull(es.getStudent().getTelephone())).forEach(this::sendSMS);

    }

    private void sendWS(EventStudent eventStudent) {
        messageService.sendToUser(eventStudent.getStudent().getLogin(), topicUser,
            new NoficationDTO(eventStudent.getEvent().getTitle(), ZonedDateTime.of(
                eventStudent.getEvent().getMetadata().getStartDate(), eventStudent.getEvent().getStart(),
                ZoneId.systemDefault())));
    }

    private void sendEmail(EventStudent eventStudent) {
        mailService.sendNotificationEmail(eventStudent.getStudent(), eventStudent.getEvent());
    }

    private void sendTelegramMessage(EventStudent eventStudent) {
        try {
            telegramSenderService.sendTextMessage(eventStudent.getStudent().getChatId(), getNotification(eventStudent));
        } catch (TelegramApiException e) {
            log.error(e.getMessage(), e);
        }
    }

    private void sendSMS(EventStudent eventStudent) {
        smsSenderService.sendSMS(eventStudent.getStudent().getTelephone(), getNotification(eventStudent));
    }

    private String getNotification(EventStudent eventStudent) {
        return String.format(NOTIFICATION, eventStudent.getEvent().getTitle(),
            LocalDateTime.of(
                eventStudent.getEvent().getMetadata().getStartDate(), eventStudent.getEvent().getStart())
                .format(DateTimeFormatter.ofPattern(DATE_TIME_FORMAT)));
    }
}
