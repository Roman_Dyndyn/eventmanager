package com.dyndyn.eventmanager.config;

import com.sendgrid.SendGrid;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Configuration class for SendGrid integration
 *
 * @author Roman Dyndyn
 */
@Configuration
public class SendGridConfiguration {

    /**
     * Creates {@link SendGrid} bean that allows access to the SendGrid API.
     */
    @Bean
    public SendGrid sendGrid(ApplicationProperties applicationProperties){
        return new SendGrid(applicationProperties.getSendgrid().getKey());
    }
}
