package com.dyndyn.eventmanager.config;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Collections;

/**
 * Configuration class for Google Calendar integration
 *
 * @author Roman Dyndyn
 */
@Configuration
public class GoogleCalendarConfiguration {

//    @Bean
//    public GoogleCredential googleCredential() throws IOException {
//        GoogleCredential credential = GoogleCredential.fromStream(new FileInputStream(new ClassPathResource("config/google/EventManager.json").getFile()))
//            .createScoped(Collections.singleton("https://www.googleapis.com/auth/calendar.events"));
//        return credential;
//    }

}

