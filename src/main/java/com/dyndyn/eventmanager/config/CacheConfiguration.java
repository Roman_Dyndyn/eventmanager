package com.dyndyn.eventmanager.config;

import java.time.Duration;

import org.ehcache.config.builders.*;
import org.ehcache.jsr107.Eh107Configuration;

import io.github.jhipster.config.jcache.BeanClassLoaderAwareJCacheRegionFactory;
import io.github.jhipster.config.JHipsterProperties;

import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.*;

@Configuration
@EnableCaching
public class CacheConfiguration {

    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        BeanClassLoaderAwareJCacheRegionFactory.setBeanClassLoader(this.getClass().getClassLoader());
        JHipsterProperties.Cache.Ehcache ehcache =
            jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofSeconds(ehcache.getTimeToLiveSeconds())))
                .build());
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            cm.createCache(com.dyndyn.eventmanager.repository.UserRepository.USERS_BY_LOGIN_CACHE, jcacheConfiguration);
            cm.createCache(com.dyndyn.eventmanager.repository.UserRepository.USERS_BY_EMAIL_CACHE, jcacheConfiguration);
            cm.createCache(com.dyndyn.eventmanager.domain.User.class.getName(), jcacheConfiguration);
            cm.createCache(com.dyndyn.eventmanager.domain.Authority.class.getName(), jcacheConfiguration);
            cm.createCache(com.dyndyn.eventmanager.domain.User.class.getName() + ".authorities", jcacheConfiguration);
            cm.createCache(com.dyndyn.eventmanager.domain.Student.class.getName(), jcacheConfiguration);
            cm.createCache(com.dyndyn.eventmanager.domain.Teacher.class.getName(), jcacheConfiguration);
            cm.createCache(com.dyndyn.eventmanager.domain.Faculty.class.getName(), jcacheConfiguration);
            cm.createCache(com.dyndyn.eventmanager.domain.Faculty.class.getName() + ".specialties", jcacheConfiguration);
            cm.createCache(com.dyndyn.eventmanager.domain.Specialty.class.getName(), jcacheConfiguration);
            cm.createCache(com.dyndyn.eventmanager.domain.Event.class.getName(), jcacheConfiguration);
            cm.createCache(com.dyndyn.eventmanager.domain.EventMetadata.class.getName(), jcacheConfiguration);
            cm.createCache(com.dyndyn.eventmanager.domain.EventStudent.class.getName(), jcacheConfiguration);
            cm.createCache(com.dyndyn.eventmanager.domain.Advertisement.class.getName(), jcacheConfiguration);
            cm.createCache(com.dyndyn.eventmanager.domain.Teacher.class.getName() + ".events", jcacheConfiguration);
            cm.createCache(com.dyndyn.eventmanager.domain.Student.class.getName() + ".eventStudents", jcacheConfiguration);
            cm.createCache(com.dyndyn.eventmanager.domain.Event.class.getName() + ".eventStudents", jcacheConfiguration);
            cm.createCache(com.dyndyn.eventmanager.domain.Event.class.getName() + ".metadata", jcacheConfiguration);
            cm.createCache(com.dyndyn.eventmanager.domain.Teacher.class.getName() + ".advertisements", jcacheConfiguration);
            cm.createCache(com.dyndyn.eventmanager.domain.Advertisement.class.getName() + ".students", jcacheConfiguration);
            cm.createCache(com.dyndyn.eventmanager.domain.Student.class.getName() + ".advertisements", jcacheConfiguration);
            cm.createCache(com.dyndyn.eventmanager.domain.Group.class.getName(), jcacheConfiguration);
            cm.createCache(com.dyndyn.eventmanager.domain.Group.class.getName() + ".students", jcacheConfiguration);
            cm.createCache(com.dyndyn.eventmanager.domain.Specialty.class.getName() + ".groups", jcacheConfiguration);
            // jhipster-needle-ehcache-add-entry
        };
    }
}
