package com.dyndyn.eventmanager.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Properties specific to Event Manager.
 * <p>
 * Properties are configured in the application.yml file.
 * See {@link io.github.jhipster.config.JHipsterProperties} for a good example.
 */
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class ApplicationProperties {

    private final Sendgrid sendgrid = new Sendgrid();
    private final Google google = new Google();
    private final Telegram telegram = new Telegram();
    private final Twilio twilio = new Twilio();

    public Telegram getTelegram() {
        return telegram;
    }

    public Twilio getTwilio() {
        return twilio;
    }

    public Sendgrid getSendgrid() {
        return sendgrid;
    }

    public Google getGoogle() {
        return google;
    }

    public static class Sendgrid {
        private String key;

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }
    }

    public static class Telegram {
        private String token;
        private String botName;

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public String getBotName() {
            return botName;
        }

        public void setBotName(String botName) {
            this.botName = botName;
        }
    }

    public static class Twilio {
        private String accountSid;

        private String authToken;

        private String phone;

        private boolean enabled;

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getAccountSid() {
            return accountSid;
        }

        public void setAccountSid(String accountSid) {
            this.accountSid = accountSid;
        }

        public String getAuthToken() {
            return authToken;
        }

        public void setAuthToken(String authToken) {
            this.authToken = authToken;
        }

        public boolean isEnabled() {
            return enabled;
        }

        public void setEnabled(boolean enabled) {
            this.enabled = enabled;
        }
    }

    public static class Google {
        private String accessTokenUrl;
        private String clientId;
        private String responseType;
        private String scope;
        private String redirectUri;
        private String accessType;
        private String grantTypeConfirm;
        private String grantTypeRefresh;
        private String confirmPermissionsUrl;
        private String clientSecret;

        public String getAccessTokenUrl() {
            return accessTokenUrl;
        }

        public void setAccessTokenUrl(String accessTokenUrl) {
            this.accessTokenUrl = accessTokenUrl;
        }

        public String getClientId() {
            return clientId;
        }

        public void setClientId(String clientId) {
            this.clientId = clientId;
        }

        public String getResponseType() {
            return responseType;
        }

        public void setResponseType(String responseType) {
            this.responseType = responseType;
        }

        public String getScope() {
            return scope;
        }

        public void setScope(String scope) {
            this.scope = scope;
        }

        public String getRedirectUri() {
            return redirectUri;
        }

        public void setRedirectUri(String redirectUri) {
            this.redirectUri = redirectUri;
        }

        public String getAccessType() {
            return accessType;
        }

        public void setAccessType(String accessType) {
            this.accessType = accessType;
        }

        public String getGrantTypeConfirm() {
            return grantTypeConfirm;
        }

        public void setGrantTypeConfirm(String grantTypeConfirm) {
            this.grantTypeConfirm = grantTypeConfirm;
        }

        public String getGrantTypeRefresh() {
            return grantTypeRefresh;
        }

        public void setGrantTypeRefresh(String grantTypeRefresh) {
            this.grantTypeRefresh = grantTypeRefresh;
        }

        public String getConfirmPermissionsUrl() {
            return confirmPermissionsUrl;
        }

        public void setConfirmPermissionsUrl(String confirmPermissionsUrl) {
            this.confirmPermissionsUrl = confirmPermissionsUrl;
        }

        public String getClientSecret() {
            return clientSecret;
        }

        public void setClientSecret(String clientSecret) {
            this.clientSecret = clientSecret;
        }
    }
}
