package com.dyndyn.eventmanager.listener;

import com.dyndyn.eventmanager.domain.Event;
import com.dyndyn.eventmanager.domain.EventMetadata;
import com.dyndyn.eventmanager.domain.EventStudent;
import com.dyndyn.eventmanager.service.GoogleCalendarService;
import com.dyndyn.eventmanager.service.TelegramSenderService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import javax.persistence.PreUpdate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static java.util.Objects.nonNull;

@Component
public class EventUpdateListener {
    private final Logger log = LoggerFactory.getLogger(EventUpdateListener.class);
    private final String NOTIFICATION_CREATED = "Подія \"%s\" була оновлена, вона розпочнеться о %s";
    private final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm";
    private static GoogleCalendarService googleCalendarService;
    private static TelegramSenderService telegramSenderService;

    @Autowired
    public void init(GoogleCalendarService googleCalendarService, TelegramSenderService telegramSenderService) {
        EventUpdateListener.googleCalendarService = googleCalendarService;
        EventUpdateListener.telegramSenderService = telegramSenderService;
    }

    @PreUpdate
    public void onUpdate(Event event) {
        event.getEventStudents().stream().filter(eventStudent -> StringUtils.isNotEmpty(eventStudent.getGoogleEventId()))
            .forEach(googleCalendarService::updateEventInUsersCalendar);
        event.getEventStudents().stream().filter(eventStudent -> eventStudent.getStudent().isTelegramEnabled() &&
            nonNull(eventStudent.getStudent().getChatId())).forEach(eventStudent -> {
            try {
                telegramSenderService.sendTextMessage(eventStudent.getStudent().getChatId(), getNotification(eventStudent));
            } catch (TelegramApiException e) {
                log.error(e.getMessage(), e);
            }
        });

    }

    private String getNotification(EventStudent eventStudent) {
        return String.format(NOTIFICATION_CREATED, eventStudent.getEvent().getTitle(),
            LocalDateTime.of(
                eventStudent.getEvent().getMetadata().getStartDate(), eventStudent.getEvent().getStart())
                .format(DateTimeFormatter.ofPattern(DATE_TIME_FORMAT)));
    }

}
