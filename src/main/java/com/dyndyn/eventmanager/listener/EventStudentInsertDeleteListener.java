package com.dyndyn.eventmanager.listener;

import com.dyndyn.eventmanager.domain.EventStudent;
import com.dyndyn.eventmanager.repository.EventStudentRepository;
import com.dyndyn.eventmanager.service.GoogleCalendarService;
import com.dyndyn.eventmanager.service.TelegramSenderService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import javax.persistence.PostPersist;
import javax.persistence.PreRemove;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static java.util.Objects.nonNull;

@Component
public class EventStudentInsertDeleteListener {
    private final Logger log = LoggerFactory.getLogger(EventStudentInsertDeleteListener.class);
    private final String NOTIFICATION_CREATED = "Створена нова подія \"%s\", яка розпочнеться о %s";
    private final String NOTIFICATION_DELETED = "Подія \"%s\" відмінена";
    private final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm";
    private static GoogleCalendarService googleCalendarService;
    private static TelegramSenderService telegramSenderService;
    private static EventStudentRepository eventStudentRepository;

    @Autowired
    public void init(GoogleCalendarService googleCalendarService, TelegramSenderService telegramSenderService,
                     EventStudentRepository eventStudentRepository) {
        EventStudentInsertDeleteListener.googleCalendarService = googleCalendarService;
        EventStudentInsertDeleteListener.telegramSenderService = telegramSenderService;
        EventStudentInsertDeleteListener.eventStudentRepository = eventStudentRepository;
    }

    @PostPersist
    @Transactional
    public void onInsert(EventStudent eventStudent) {
        eventStudent = eventStudentRepository.getOne(eventStudent.getId());
        googleCalendarService.addEventToUsersCalendar(eventStudent);
        if (eventStudent.getStudent().isTelegramEnabled() &&
            nonNull(eventStudent.getStudent().getChatId())) {
            try {
                telegramSenderService.sendTextMessage(eventStudent.getStudent().getChatId(), getNotification(eventStudent));
            } catch (TelegramApiException e) {
                log.error(e.getMessage(), e);
            }
        }
    }

    @PreRemove
    public void onRemove(EventStudent eventStudent) {
        if (StringUtils.isNotEmpty(eventStudent.getGoogleEventId())) {
            googleCalendarService.removeEventFromUserCalendar(eventStudent);
        }
        if (eventStudent.getStudent().isTelegramEnabled() &&
            nonNull(eventStudent.getStudent().getChatId())){
            try {
                telegramSenderService.sendTextMessage(eventStudent.getStudent().getChatId(), String.format(NOTIFICATION_DELETED,
                    eventStudent.getEvent().getTitle()));
            } catch (TelegramApiException e) {
                log.error(e.getMessage(), e);
            }
        }
    }

    private String getNotification(EventStudent eventStudent) {
        return String.format(NOTIFICATION_CREATED, eventStudent.getEvent().getTitle(),
            LocalDateTime.of(
                eventStudent.getEvent().getMetadata().getStartDate(), eventStudent.getEvent().getStart())
                .format(DateTimeFormatter.ofPattern(DATE_TIME_FORMAT)));
    }

}
