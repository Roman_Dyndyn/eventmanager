package com.dyndyn.eventmanager.listener;

import com.dyndyn.eventmanager.domain.EventMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.PreUpdate;

@Component
public class EventMetadataUpdateListener {
    private final Logger log = LoggerFactory.getLogger(EventMetadataUpdateListener.class);
    private static EventUpdateListener eventUpdateListener;
    @Autowired
    public void init(EventUpdateListener eventUpdateListener) {
        EventMetadataUpdateListener.eventUpdateListener = eventUpdateListener;
    }

    @PreUpdate
    public void onUpdate(EventMetadata event) {
        eventUpdateListener.onUpdate(event.getEvent());
    }
}
