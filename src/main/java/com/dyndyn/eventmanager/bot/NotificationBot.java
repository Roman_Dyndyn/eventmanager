package com.dyndyn.eventmanager.bot;

import com.dyndyn.eventmanager.config.ApplicationProperties;
import com.dyndyn.eventmanager.domain.User;
import com.dyndyn.eventmanager.service.EventService;
import com.dyndyn.eventmanager.service.TelegramSenderService;
import com.dyndyn.eventmanager.service.UserService;
import com.dyndyn.eventmanager.service.dto.EventDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

@Component
@Profile("!test")
public class NotificationBot extends TelegramLongPollingBot {

    private static final Logger log = LoggerFactory.getLogger(NotificationBot.class);
    private static final String COMMAND_NOT_RECOGNIZED = "Команда не розпізнана";
    private static final String USERNAME_FOUND = "%s, тепер Ви будете отримувати нагадування";
    private static final String USERNAME_NOT_FOUND = "Такого користувача не існує, спробуйти ввести логін ще раз";
    private static final String EVENT = "\"%s\" - %s %s-%s ";

    private final Map<Long, BotCommand> users = new HashMap<>();
    private ApplicationProperties applicationProperties;
    private TelegramSenderService telegramSenderService;
    private UserService userService;
    private EventService eventService;

    public NotificationBot(ApplicationProperties applicationProperties, TelegramSenderService telegramSenderService,
                           UserService userService, EventService eventService) {
        this.applicationProperties = applicationProperties;
        this.telegramSenderService = telegramSenderService;
        this.userService = userService;
        this.eventService = eventService;
    }

    @Override
    public void onUpdateReceived(Update update) {
        try {
            Long chatId = update.getMessage().getChatId();
            String text = update.getMessage().getText();
            if (text.equalsIgnoreCase(BotCommand.START.command) ||
                text.equalsIgnoreCase(BotCommand.USERNAME.command)) {
                telegramSenderService.sendTextMessage(chatId, BotCommand.USERNAME.message);
                users.put(chatId, BotCommand.USERNAME);
            } else if (text.equalsIgnoreCase(BotCommand.DISABLE.command)) {
                telegramSenderService.sendTextMessage(chatId, BotCommand.DISABLE.message);
                users.put(chatId, BotCommand.DISABLE);
            } else if (text.equalsIgnoreCase(BotCommand.ENABLE.command)) {
                telegramSenderService.sendTextMessage(chatId, BotCommand.ENABLE.message);
                users.put(chatId, BotCommand.ENABLE);
            } else if (text.equalsIgnoreCase(BotCommand.EVENTS.command)) {
                telegramSenderService.sendTextMessage(chatId, getEventList(chatId));
            } else if (users.get(chatId) == BotCommand.USERNAME) {
                Optional<User> user = userService.setTelegramChatId(text, chatId);
                if (user.isPresent()) {
                    users.remove(chatId);
                    telegramSenderService.sendTextMessage(chatId, String.format(USERNAME_FOUND, user.get().getFirstName()));
                } else {
                    telegramSenderService.sendTextMessage(chatId, USERNAME_NOT_FOUND);
                }
            } else {
                telegramSenderService.sendTextMessage(chatId, COMMAND_NOT_RECOGNIZED);
            }
        } catch (TelegramApiException e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public String getBotUsername() {
        return applicationProperties.getTelegram().getBotName();
    }

    @Override
    public String getBotToken() {
        return applicationProperties.getTelegram().getToken();
    }

    private String getEventList(Long chatId) {
        return eventService.findAllByStudentChatId(chatId).stream().flatMap(this::getAllEvents)
            .filter(eventDTO -> eventDTO.getRepeatStart().isAfter(LocalDate.now().minusDays(1)))
            .sorted(Comparator.comparing(EventDTO::getRepeatStart).thenComparing(EventDTO::getStart))
            .map(eventDTO -> String.format(EVENT, eventDTO.getTitle(),
                eventDTO.getRepeatStart().format(DateTimeFormatter.ISO_DATE),
                eventDTO.getStart().format(DateTimeFormatter.ISO_TIME),
                eventDTO.getEnd().format(DateTimeFormatter.ISO_TIME))).collect(Collectors.joining(",\n"));
    }

    private Stream<EventDTO> getAllEvents(EventDTO eventDTO) {
        if (Objects.isNull(eventDTO.getRepeatEnd())) {
            if (eventDTO.getRepeatStart().isAfter(LocalDate.now().minusDays(1))) {
                return Stream.of(eventDTO);
            } else {
                return Stream.empty();
            }
        }
        Period daysBetweenStartAndEnd = Period.between(eventDTO.getRepeatStart(), eventDTO.getRepeatEnd());
        Period interval = Period.ofDays(eventDTO.getInterval());
        return IntStream.range(1, daysBetweenStartAndEnd.getDays() / eventDTO.getInterval() + 1).
            mapToObj(i -> copyEventDTO(eventDTO, interval.multipliedBy(i)));
    }

    private EventDTO copyEventDTO(EventDTO eventDTO, Period period) {
        EventDTO result = new EventDTO();
        result.setTitle(eventDTO.getTitle());
        result.setRepeatStart(eventDTO.getRepeatStart().plus(period));
        result.setStart(eventDTO.getStart());
        result.setEnd(eventDTO.getEnd());
        result.setId(eventDTO.getId());
        result.setDescription(eventDTO.getDescription());
        result.setTeacherName(eventDTO.getTeacherName());
        return result;
    }
}
