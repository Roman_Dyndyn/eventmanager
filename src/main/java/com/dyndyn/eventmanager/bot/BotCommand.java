package com.dyndyn.eventmanager.bot;

public enum BotCommand {
    START("/start", "Будь ласка, введіть свій логін"),
    USERNAME("/username", "Будь ласка, введіть свій логін"),
    DISABLE("/disable", "Нагадування відключено"),
    ENABLE ("/enable", "Нагадування підключено"),
    EVENTS ("/events", "Список Подій");

    public final String command;
    public final String message;

    BotCommand(String command, String message) {
        this.command = command;
        this.message = message;
    }
}
