package com.dyndyn.eventmanager.repository;

import com.dyndyn.eventmanager.domain.Advertisement;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Advertisement entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AdvertisementRepository extends JpaRepository<Advertisement, Long>, JpaSpecificationExecutor<Advertisement> {

    @Query(value = "select distinct advertisement from Advertisement advertisement left join fetch advertisement.students",
        countQuery = "select count(distinct advertisement) from Advertisement advertisement")
    Page<Advertisement> findAllWithEagerRelationships(Pageable pageable);

    @Query(value = "select distinct advertisement from Advertisement advertisement left join fetch advertisement.students")
    List<Advertisement> findAllWithEagerRelationships();

    @Query("select advertisement from Advertisement advertisement left join fetch advertisement.students where advertisement.id =:id")
    Optional<Advertisement> findOneWithEagerRelationships(@Param("id") Long id);

}
