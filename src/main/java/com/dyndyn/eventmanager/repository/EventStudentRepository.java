package com.dyndyn.eventmanager.repository;

import com.dyndyn.eventmanager.domain.EventStudent;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the EventStudent entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EventStudentRepository extends JpaRepository<EventStudent, Long>, JpaSpecificationExecutor<EventStudent> {

}
