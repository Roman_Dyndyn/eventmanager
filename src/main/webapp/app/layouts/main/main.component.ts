import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router, ActivatedRouteSnapshot, NavigationEnd } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';

import { JhiLanguageHelper, JhiTrackerService, Principal } from 'app/core';
import { NgbdModalComponent } from 'app/core/popup/dialog.component';

@Component({
    selector: 'jhi-main',
    templateUrl: './main.component.html'
})
export class JhiMainComponent implements OnInit, OnDestroy {
    constructor(
        private jhiLanguageHelper: JhiLanguageHelper,
        private router: Router,
        private principal: Principal,
        private trackerService: JhiTrackerService,
        private modalService: NgbModal
    ) {}

    private getPageTitle(routeSnapshot: ActivatedRouteSnapshot) {
        let title: string = routeSnapshot.data && routeSnapshot.data['pageTitle'] ? routeSnapshot.data['pageTitle'] : 'eventManagerApp';
        if (routeSnapshot.firstChild) {
            title = this.getPageTitle(routeSnapshot.firstChild) || title;
        }
        return title;
    }

    ngOnInit() {
        this.router.events.subscribe(event => {
            if (event instanceof NavigationEnd) {
                this.jhiLanguageHelper.updateTitle(this.getPageTitle(this.router.routerState.snapshot.root));
            }
        });
        this.principal.getAuthenticationState().subscribe(identity => {
            console.log(identity);
            if (this.principal.isAuthenticated()) {
                // Subscribe for notifications
                this.trackerService.subscribeOnNotification();
                this.trackerService.receiveNotification().subscribe(event => this.showEvent(event));
            } else {
                this.trackerService.unsubscribeFormNotification();
            }
        });
    }

    ngOnDestroy() {
        this.trackerService.unsubscribeFormNotification();
    }

    private showEvent(event: any) {
        const modalRef = this.modalService.open(NgbdModalComponent);
        modalRef.componentInstance.title = event.title;
        modalRef.componentInstance.start = event.start != null ? moment(event.start) : null;
    }
}
