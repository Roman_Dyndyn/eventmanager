import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EventManagerSharedModule } from 'app/shared';

import {
    PasswordStrengthBarComponent,
    RegisterComponent,
    ActivateComponent,
    PasswordComponent,
    PasswordResetInitComponent,
    PasswordResetFinishComponent,
    SettingsComponent,
    accountState
} from './';

import { GoogleApiModule, GoogleApiService, GoogleAuthService, NG_GAPI_CONFIG, GoogleApiConfig } from 'ng-gapi';
import ClientConfig = gapi.auth2.ClientConfig;

const gapiClientConfig: ClientConfig = {
    client_id: '378592017956-gj0g243u45kgvfmenp60a517t2dtil0k.apps.googleusercontent.com',
    scope: ['https://www.googleapis.com/auth/calendar.events'].join(' '),
    fetch_basic_profile: false
};

@NgModule({
    imports: [
        EventManagerSharedModule,
        RouterModule.forChild(accountState),
        GoogleApiModule.forRoot({
            provide: NG_GAPI_CONFIG,
            useValue: gapiClientConfig
        })
    ],
    declarations: [
        ActivateComponent,
        RegisterComponent,
        PasswordComponent,
        PasswordStrengthBarComponent,
        PasswordResetInitComponent,
        PasswordResetFinishComponent,
        SettingsComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class EventManagerAccountModule {}
