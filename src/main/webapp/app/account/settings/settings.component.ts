import { Component, OnInit } from '@angular/core';
import { JhiAlertService, JhiLanguageService } from 'ng-jhipster';

import { Principal, AccountService, JhiLanguageHelper } from 'app/core';
import moment = require('moment');
import { StudentService } from 'app/entities/student';
import { Observable } from 'rxjs';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { TeacherService } from 'app/entities/teacher';
import { IGroup } from 'app/shared/model/group.model';
import { GroupService } from 'app/entities/group';

@Component({
    selector: 'jhi-settings',
    templateUrl: './settings.component.html'
})
export class SettingsComponent implements OnInit {
    error: string;
    success: string;
    settingsAccount: any;
    languages: any[];

    groups: IGroup[];
    birthdayDp: any;

    constructor(
        private account: AccountService,
        private principal: Principal,
        private languageService: JhiLanguageService,
        private languageHelper: JhiLanguageHelper,
        private jhiAlertService: JhiAlertService,
        private groupService: GroupService,
        private studentService: StudentService,
        private teacherService: TeacherService
    ) {}

    ngOnInit() {
        this.principal.identity().then(account => {
            this.settingsAccount = this.copyAccount(account);
        });
        this.languageHelper.getAll().then(languages => {
            this.languages = languages;
        });

        this.groupService.query().subscribe(
            (res: HttpResponse<IGroup[]>) => {
                this.groups = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    save() {
        let response: Observable<HttpResponse<any>>;

        if (this.principal.hasAnyAuthorityDirect(['ROLE_STUDENT'])) {
            response = this.studentService.update(this.settingsAccount);
        } else if (this.principal.hasAnyAuthorityDirect(['ROLE_TEACHER'])) {
            response = this.teacherService.update(this.settingsAccount);
        } else {
            response = this.account.save(this.settingsAccount);
        }
        response.subscribe(
            () => {
                this.error = null;
                this.success = 'OK';
                this.principal.identity(true).then(account => {
                    this.settingsAccount = this.copyAccount(account);
                });
                this.languageService.getCurrent().then(current => {
                    if (this.settingsAccount.langKey !== current) {
                        this.languageService.changeLanguage(this.settingsAccount.langKey);
                    }
                });
            },
            () => {
                this.success = null;
                this.error = 'ERROR';
            }
        );
    }

    copyAccount(account) {
        return {
            id: account.id,
            activated: account.activated,
            email: account.email,
            firstName: account.firstName,
            langKey: account.langKey,
            lastName: account.lastName,
            login: account.login,
            imageUrl: account.imageUrl,
            birthday: account.birthday != null ? moment(account.birthday) : null,
            telephone: account.telephone,
            address: account.address,
            groupId: account.groupId
        };
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
