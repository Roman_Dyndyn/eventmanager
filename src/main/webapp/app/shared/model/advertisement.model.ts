import { IStudent } from 'app/shared/model//student.model';

export interface IAdvertisement {
    id?: number;
    title?: string;
    description?: any;
    teacherName?: string;
    teacherId?: number;
    students?: IStudent[];
}

export class Advertisement implements IAdvertisement {
    constructor(
        public id?: number,
        public title?: string,
        public description?: any,
        public teacherName?: string,
        public teacherId?: number,
        public students?: IStudent[]
    ) {}
}
