import { IGroup } from 'app/shared/model//group.model';

export interface ISpecialty {
    id?: number;
    name?: string;
    enabled?: boolean;
    facultyName?: string;
    facultyId?: number;
    groups?: IGroup[];
}

export class Specialty implements ISpecialty {
    constructor(
        public id?: number,
        public name?: string,
        public enabled?: boolean,
        public facultyName?: string,
        public facultyId?: number,
        public groups?: IGroup[]
    ) {
        this.enabled = this.enabled || false;
    }
}
