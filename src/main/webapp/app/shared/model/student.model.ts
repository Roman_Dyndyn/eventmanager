import { Moment } from 'moment';
import { IEventStudent } from 'app/shared/model//event-student.model';
import { IAdvertisement } from 'app/shared/model//advertisement.model';

export interface IStudent {
    id?: number;
    email?: string;
    firstName?: string;
    lastName?: string;
    login?: string;
    birthday?: Moment;
    telephone?: string;
    address?: string;
    activated?: boolean;
    langKey?: string;
    imageUrl?: string;
    eventStudents?: IEventStudent[];
    advertisements?: IAdvertisement[];
    groupName?: string;
    groupId?: number;
}

export class Student implements IStudent {
    constructor(
        public id?: number,
        public email?: string,
        public firstName?: string,
        public lastName?: string,
        public login?: string,
        public birthday?: Moment,
        public telephone?: string,
        public address?: string,
        public activated?: boolean,
        public langKey?: string,
        public imageUrl?: string,
        public eventStudents?: IEventStudent[],
        public advertisements?: IAdvertisement[],
        public groupName?: string,
        public groupId?: number
    ) {}
}
