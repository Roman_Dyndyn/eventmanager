import { ISpecialty } from 'app/shared/model//specialty.model';

export interface IFaculty {
    id?: number;
    name?: string;
    enabled?: boolean;
    specialties?: ISpecialty[];
}

export class Faculty implements IFaculty {
    constructor(public id?: number, public name?: string, public enabled?: boolean, public specialties?: ISpecialty[]) {
        this.enabled = this.enabled || false;
    }
}
