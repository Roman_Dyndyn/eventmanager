import { Moment } from 'moment';
import { IEvent } from 'app/shared/model//event.model';
import { IAdvertisement } from 'app/shared/model//advertisement.model';

export interface ITeacher {
    id?: number;
    email?: string;
    firstName?: string;
    lastName?: string;
    login?: string;
    birthday?: Moment;
    telephone?: string;
    address?: string;
    activated?: boolean;
    langKey?: string;
    imageUrl?: string;
    events?: IEvent[];
    advertisements?: IAdvertisement[];
}

export class Teacher implements ITeacher {
    constructor(
        public id?: number,
        public email?: string,
        public firstName?: string,
        public lastName?: string,
        public login?: string,
        public birthday?: Moment,
        public telephone?: string,
        public address?: string,
        public activated?: boolean,
        public langKey?: string,
        public imageUrl?: string,
        public events?: IEvent[],
        public advertisements?: IAdvertisement[]
    ) {}
}
