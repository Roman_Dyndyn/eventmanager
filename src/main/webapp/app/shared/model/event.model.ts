import { Moment } from 'moment';

export interface IEvent {
    id?: number;
    title?: string;
    description?: any;
    start?: string;
    end?: string;
    teacherName?: string;
    teacherId?: number;
    studentIds?: number[];
    repeatStart?: Moment;
    repeatEnd?: Moment;
    metadataId?: number;
    interval?: number;
}

export class Event implements IEvent {
    constructor(
        public id?: number,
        public title?: string,
        public description?: any,
        public start?: string,
        public end?: string,
        public teacherName?: string,
        public teacherId?: number,
        public studentIds?: number[],
        public repeatStart?: Moment,
        public repeatEnd?: Moment,
        public metadataId?: number,
        public interval?: number
    ) {}
}
