import { IStudent } from 'app/shared/model//student.model';

export interface IGroup {
    id?: number;
    name?: string;
    enabled?: boolean;
    specialtyName?: string;
    specialtyId?: number;
    students?: IStudent[];
}

export class Group implements IGroup {
    constructor(
        public id?: number,
        public name?: string,
        public enabled?: boolean,
        public specialtyName?: string,
        public specialtyId?: number,
        public students?: IStudent[]
    ) {
        this.enabled = this.enabled || false;
    }
}
