export interface IEventStudent {
    id?: number;
    minutes?: number;
    googleEventId?: string;
    studentName?: string;
    studentId?: number;
    eventTitle?: string;
    eventId?: number;
}

export class EventStudent implements IEventStudent {
    constructor(
        public id?: number,
        public minutes?: number,
        public googleEventId?: string,
        public studentName?: string,
        public studentId?: number,
        public eventTitle?: string,
        public eventId?: number
    ) {}
}
