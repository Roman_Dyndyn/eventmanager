import { NgModule, LOCALE_ID } from '@angular/core';
import { DatePipe, registerLocaleData } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { Title } from '@angular/platform-browser';
import locale from '@angular/common/locales/uk';
import { NgbdModalComponent } from 'app/core/popup/dialog.component';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
    imports: [HttpClientModule, NgbModalModule],
    exports: [],
    declarations: [NgbdModalComponent],
    providers: [
        Title,
        {
            provide: LOCALE_ID,
            useValue: 'uk'
        },
        DatePipe
    ],
    entryComponents: [NgbdModalComponent]
})
export class EventManagerCoreModule {
    constructor() {
        registerLocaleData(locale);
    }
}
