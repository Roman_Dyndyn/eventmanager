import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Moment } from 'moment';

@Component({
    selector: 'jhi-ngbd-modal-component',
    templateUrl: './dialog.component.html'
})
export class NgbdModalComponent {
    title: string;
    start: Moment;

    constructor(public activeModal: NgbActiveModal) {}
}
