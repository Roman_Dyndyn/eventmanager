import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IEventStudent } from 'app/shared/model/event-student.model';

@Component({
    selector: 'jhi-event-student-detail',
    templateUrl: './event-student-detail.component.html'
})
export class EventStudentDetailComponent implements OnInit {
    eventStudent: IEventStudent;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ eventStudent }) => {
            this.eventStudent = eventStudent;
        });
    }

    previousState() {
        window.history.back();
    }
}
