export * from './event-student.service';
export * from './event-student-update.component';
export * from './event-student-delete-dialog.component';
export * from './event-student-detail.component';
export * from './event-student.component';
export * from './event-student.route';
