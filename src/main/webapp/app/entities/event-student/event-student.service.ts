import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IEventStudent } from 'app/shared/model/event-student.model';

type EntityResponseType = HttpResponse<IEventStudent>;
type EntityArrayResponseType = HttpResponse<IEventStudent[]>;

@Injectable({ providedIn: 'root' })
export class EventStudentService {
    public resourceUrl = SERVER_API_URL + 'api/event-students';

    constructor(private http: HttpClient) {}

    create(eventStudent: IEventStudent): Observable<EntityResponseType> {
        return this.http.post<IEventStudent>(this.resourceUrl, eventStudent, { observe: 'response' });
    }

    update(eventStudent: IEventStudent): Observable<EntityResponseType> {
        return this.http.put<IEventStudent>(this.resourceUrl, eventStudent, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IEventStudent>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IEventStudent[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
