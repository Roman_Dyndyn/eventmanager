import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { IEventStudent } from 'app/shared/model/event-student.model';
import { EventStudentService } from './event-student.service';
import { IStudent } from 'app/shared/model/student.model';
import { StudentService } from 'app/entities/student';
import { IEvent } from 'app/shared/model/event.model';
import { EventService } from 'app/entities/event';

@Component({
    selector: 'jhi-event-student-update',
    templateUrl: './event-student-update.component.html'
})
export class EventStudentUpdateComponent implements OnInit {
    eventStudent: IEventStudent;
    isSaving: boolean;

    students: IStudent[];

    events: IEvent[];

    constructor(
        private jhiAlertService: JhiAlertService,
        private eventStudentService: EventStudentService,
        private studentService: StudentService,
        private eventService: EventService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ eventStudent }) => {
            this.eventStudent = eventStudent;
        });
        this.studentService.query().subscribe(
            (res: HttpResponse<IStudent[]>) => {
                this.students = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.eventService.query().subscribe(
            (res: HttpResponse<IEvent[]>) => {
                this.events = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.eventStudent.id !== undefined) {
            this.subscribeToSaveResponse(this.eventStudentService.update(this.eventStudent));
        } else {
            this.subscribeToSaveResponse(this.eventStudentService.create(this.eventStudent));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IEventStudent>>) {
        result.subscribe((res: HttpResponse<IEventStudent>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackStudentById(index: number, item: IStudent) {
        return item.id;
    }

    trackEventById(index: number, item: IEvent) {
        return item.id;
    }
}
