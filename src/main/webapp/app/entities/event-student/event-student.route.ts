import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { EventStudent } from 'app/shared/model/event-student.model';
import { EventStudentService } from './event-student.service';
import { EventStudentComponent } from './event-student.component';
import { EventStudentDetailComponent } from './event-student-detail.component';
import { EventStudentUpdateComponent } from './event-student-update.component';
import { EventStudentDeletePopupComponent } from './event-student-delete-dialog.component';
import { IEventStudent } from 'app/shared/model/event-student.model';

@Injectable({ providedIn: 'root' })
export class EventStudentResolve implements Resolve<IEventStudent> {
    constructor(private service: EventStudentService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((eventStudent: HttpResponse<EventStudent>) => eventStudent.body));
        }
        return of(new EventStudent());
    }
}

export const eventStudentRoute: Routes = [
    {
        path: 'event-student',
        component: EventStudentComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_TEACHER', 'ROLE_ADMIN', 'ROLE_STUDENT'],
            pageTitle: 'eventManagerApp.eventStudent.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'event-student/:id/view',
        component: EventStudentDetailComponent,
        resolve: {
            eventStudent: EventStudentResolve
        },
        data: {
            authorities: ['ROLE_TEACHER', 'ROLE_ADMIN', 'ROLE_STUDENT'],
            pageTitle: 'eventManagerApp.eventStudent.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'event-student/new',
        component: EventStudentUpdateComponent,
        resolve: {
            eventStudent: EventStudentResolve
        },
        data: {
            authorities: ['ROLE_TEACHER', 'ROLE_ADMIN', 'ROLE_STUDENT'],
            pageTitle: 'eventManagerApp.eventStudent.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'event-student/:id/edit',
        component: EventStudentUpdateComponent,
        resolve: {
            eventStudent: EventStudentResolve
        },
        data: {
            authorities: ['ROLE_TEACHER', 'ROLE_ADMIN', 'ROLE_STUDENT'],
            pageTitle: 'eventManagerApp.eventStudent.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const eventStudentPopupRoute: Routes = [
    {
        path: 'event-student/:id/delete',
        component: EventStudentDeletePopupComponent,
        resolve: {
            eventStudent: EventStudentResolve
        },
        data: {
            authorities: ['ROLE_TEACHER', 'ROLE_ADMIN', 'ROLE_STUDENT'],
            pageTitle: 'eventManagerApp.eventStudent.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
