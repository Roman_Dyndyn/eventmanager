import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EventManagerSharedModule } from 'app/shared';
import {
    EventStudentComponent,
    EventStudentDetailComponent,
    EventStudentUpdateComponent,
    EventStudentDeletePopupComponent,
    EventStudentDeleteDialogComponent,
    eventStudentRoute,
    eventStudentPopupRoute
} from './';

const ENTITY_STATES = [...eventStudentRoute, ...eventStudentPopupRoute];

@NgModule({
    imports: [EventManagerSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        EventStudentComponent,
        EventStudentDetailComponent,
        EventStudentUpdateComponent,
        EventStudentDeleteDialogComponent,
        EventStudentDeletePopupComponent
    ],
    entryComponents: [
        EventStudentComponent,
        EventStudentUpdateComponent,
        EventStudentDeleteDialogComponent,
        EventStudentDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class EventManagerEventStudentModule {}
