import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Advertisement } from 'app/shared/model/advertisement.model';
import { AdvertisementService } from './advertisement.service';
import { AdvertisementComponent } from './advertisement.component';
import { AdvertisementDetailComponent } from './advertisement-detail.component';
import { AdvertisementUpdateComponent } from './advertisement-update.component';
import { AdvertisementDeletePopupComponent } from './advertisement-delete-dialog.component';
import { IAdvertisement } from 'app/shared/model/advertisement.model';

@Injectable({ providedIn: 'root' })
export class AdvertisementResolve implements Resolve<IAdvertisement> {
    constructor(private service: AdvertisementService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((advertisement: HttpResponse<Advertisement>) => advertisement.body));
        }
        return of(new Advertisement());
    }
}

export const advertisementRoute: Routes = [
    {
        path: 'advertisement',
        component: AdvertisementComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_TEACHER', 'ROLE_ADMIN', 'ROLE_STUDENT'],
            pageTitle: 'eventManagerApp.advertisement.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'advertisement/:id/view',
        component: AdvertisementDetailComponent,
        resolve: {
            advertisement: AdvertisementResolve
        },
        data: {
            authorities: ['ROLE_TEACHER', 'ROLE_ADMIN', 'ROLE_STUDENT'],
            pageTitle: 'eventManagerApp.advertisement.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'advertisement/new',
        component: AdvertisementUpdateComponent,
        resolve: {
            advertisement: AdvertisementResolve
        },
        data: {
            authorities: ['ROLE_TEACHER', 'ROLE_ADMIN'],
            pageTitle: 'eventManagerApp.advertisement.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'advertisement/:id/edit',
        component: AdvertisementUpdateComponent,
        resolve: {
            advertisement: AdvertisementResolve
        },
        data: {
            authorities: ['ROLE_TEACHER', 'ROLE_ADMIN'],
            pageTitle: 'eventManagerApp.advertisement.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const advertisementPopupRoute: Routes = [
    {
        path: 'advertisement/:id/delete',
        component: AdvertisementDeletePopupComponent,
        resolve: {
            advertisement: AdvertisementResolve
        },
        data: {
            authorities: ['ROLE_TEACHER', 'ROLE_ADMIN'],
            pageTitle: 'eventManagerApp.advertisement.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
