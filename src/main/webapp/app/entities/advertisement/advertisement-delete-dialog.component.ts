import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IAdvertisement } from 'app/shared/model/advertisement.model';
import { AdvertisementService } from './advertisement.service';

@Component({
    selector: 'jhi-advertisement-delete-dialog',
    templateUrl: './advertisement-delete-dialog.component.html'
})
export class AdvertisementDeleteDialogComponent {
    advertisement: IAdvertisement;

    constructor(
        private advertisementService: AdvertisementService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.advertisementService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'advertisementListModification',
                content: 'Deleted an advertisement'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-advertisement-delete-popup',
    template: ''
})
export class AdvertisementDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ advertisement }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(AdvertisementDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.advertisement = advertisement;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
