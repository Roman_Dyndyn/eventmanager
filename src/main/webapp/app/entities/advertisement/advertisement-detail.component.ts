import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiDataUtils } from 'ng-jhipster';

import { IAdvertisement } from 'app/shared/model/advertisement.model';

@Component({
    selector: 'jhi-advertisement-detail',
    templateUrl: './advertisement-detail.component.html'
})
export class AdvertisementDetailComponent implements OnInit {
    advertisement: IAdvertisement;

    constructor(private dataUtils: JhiDataUtils, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ advertisement }) => {
            this.advertisement = advertisement;
        });
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    previousState() {
        window.history.back();
    }
}
