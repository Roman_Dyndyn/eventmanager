import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EventManagerSharedModule } from 'app/shared';
import {
    AdvertisementComponent,
    AdvertisementDetailComponent,
    AdvertisementUpdateComponent,
    AdvertisementDeletePopupComponent,
    AdvertisementDeleteDialogComponent,
    advertisementRoute,
    advertisementPopupRoute
} from './';

const ENTITY_STATES = [...advertisementRoute, ...advertisementPopupRoute];

@NgModule({
    imports: [EventManagerSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        AdvertisementComponent,
        AdvertisementDetailComponent,
        AdvertisementUpdateComponent,
        AdvertisementDeleteDialogComponent,
        AdvertisementDeletePopupComponent
    ],
    entryComponents: [
        AdvertisementComponent,
        AdvertisementUpdateComponent,
        AdvertisementDeleteDialogComponent,
        AdvertisementDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class EventManagerAdvertisementModule {}
