import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IAdvertisement } from 'app/shared/model/advertisement.model';

type EntityResponseType = HttpResponse<IAdvertisement>;
type EntityArrayResponseType = HttpResponse<IAdvertisement[]>;

@Injectable({ providedIn: 'root' })
export class AdvertisementService {
    public resourceUrl = SERVER_API_URL + 'api/advertisements';

    constructor(private http: HttpClient) {}

    create(advertisement: IAdvertisement): Observable<EntityResponseType> {
        return this.http.post<IAdvertisement>(this.resourceUrl, advertisement, { observe: 'response' });
    }

    update(advertisement: IAdvertisement): Observable<EntityResponseType> {
        return this.http.put<IAdvertisement>(this.resourceUrl, advertisement, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IAdvertisement>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IAdvertisement[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
