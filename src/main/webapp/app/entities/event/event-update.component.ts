import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { IEvent } from 'app/shared/model/event.model';
import { EventService } from './event.service';
import { ITeacher } from 'app/shared/model/teacher.model';
import { TeacherService } from 'app/entities/teacher';
import { IActionMapping, ITreeOptions, TREE_ACTIONS, TreeComponent, TreeNode, TreeModel } from 'angular-tree-component';
import { IFaculty } from 'app/shared/model/faculty.model';
import { FacultyService } from 'app/entities/faculty';
import { SpecialtyService } from 'app/entities/specialty';
import { GroupService } from 'app/entities/group';
import { map } from 'rxjs/operators';
import { ISpecialty } from 'app/shared/model/specialty.model';
import { IGroup } from 'app/shared/model/group.model';
import { IStudent } from 'app/shared/model/student.model';

const actMapping: IActionMapping = {
    mouse: {
        click: (tree, node, $event) => {
            if (node.hasChildren) {
                TREE_ACTIONS.TOGGLE_EXPANDED(tree, node, $event);
            }
        },
        checkboxClick: (tree, node, $event) => {
            if (node.hasChildren && !node.isExpanded && !node.isSelected && node.children === undefined) {
                EventUpdateComponent.loadAndSelect(tree, node, $event);
            }
            TREE_ACTIONS.TOGGLE_SELECTED(tree, node, $event);
        }
    }
};

@Component({
    selector: 'jhi-event-update',
    templateUrl: './event-update.component.html',
    styleUrls: ['./event-update.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class EventUpdateComponent implements OnInit {
    event: IEvent;
    isSaving: boolean;

    teachers: ITeacher[];
    repeatable: boolean;

    @ViewChild('tree') treeComponent: TreeComponent;
    nodes = [];
    options: ITreeOptions = {
        useCheckbox: true,
        getChildren: this.getChildren.bind(this),
        actionMapping: actMapping
    };
    selected: Set<number>;

    public static loadAndSelect(tree: TreeModel, node: TreeNode, $event: any) {
        new Promise(() => {
            node.loadNodeChildren().then(() => {
                for (const child of node.children) {
                    if (child.hasChildren) {
                        this.loadAndSelect(tree, child, $event);
                    } else {
                        TREE_ACTIONS.TOGGLE_SELECTED(tree, child, $event);
                    }
                }
            });
        }).then(() => node.expand());
    }

    constructor(
        private dataUtils: JhiDataUtils,
        private jhiAlertService: JhiAlertService,
        private eventService: EventService,
        private teacherService: TeacherService,
        private activatedRoute: ActivatedRoute,
        private facultyService: FacultyService,
        private specialtyService: SpecialtyService,
        private groupService: GroupService
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ event }) => {
            this.event = event;
            this.repeatable = this.event.repeatEnd != null;
        });
        this.teacherService.query().subscribe(
            (res: HttpResponse<ITeacher[]>) => {
                this.teachers = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.facultyService.query().subscribe((res: HttpResponse<IFaculty[]>) => {
            this.nodes = res.body.map(faculty => {
                return {
                    hasChildren: true,
                    name: faculty.name,
                    id: faculty.id,
                    type: 'faculty'
                };
            });
        });
        this.selected = new Set<number>(this.event.studentIds);
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        this.event.studentIds = Array.from(this.selected);
        if (!this.repeatable) {
            this.event.repeatEnd = null;
            this.event.interval = 1;
        }
        if (this.event.interval === undefined || this.event.interval === 0) {
            this.event.interval = 1;
        }

        if (this.event.id !== undefined) {
            this.subscribeToSaveResponse(this.eventService.update(this.event));
        } else {
            this.subscribeToSaveResponse(this.eventService.create(this.event));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IEvent>>) {
        result.subscribe((res: HttpResponse<IEvent>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    private getChildren(node: TreeNode) {
        switch (node.data.type) {
            case 'faculty':
                return this.facultyService
                    .findSpecialtiesByFacultyId(node.data.id)
                    .pipe(
                        map((res: HttpResponse<ISpecialty[]>) => res.body),
                        map((specialties: ISpecialty[]) =>
                            specialties.map((specialty: ISpecialty) => {
                                return {
                                    hasChildren: true,
                                    name: specialty.name,
                                    id: specialty.id,
                                    type: 'specialty'
                                };
                            })
                        )
                    )
                    .toPromise();
            case 'specialty':
                return this.specialtyService
                    .findGroupsBySpecialtyId(node.data.id)
                    .pipe(
                        map((res: HttpResponse<IGroup[]>) => res.body),
                        map((groups: IGroup[]) =>
                            groups.map((group: IGroup) => {
                                return {
                                    hasChildren: true,
                                    name: group.name,
                                    id: group.id,
                                    type: 'group'
                                };
                            })
                        )
                    )
                    .toPromise();
            case 'group':
                return this.groupService
                    .getStudentsByGroupId(node.data.id)
                    .pipe(
                        map((res: HttpResponse<IStudent[]>) => res.body),
                        map((students: IStudent[]) =>
                            students.map((student: IStudent) => {
                                return {
                                    hasChildren: false,
                                    name: student.firstName + ' ' + student.lastName,
                                    id: student.id,
                                    type: 'student'
                                };
                            })
                        )
                    )
                    .toPromise();
        }
    }

    trackTeacherById(index: number, item: ITeacher) {
        return item.id;
    }

    updateTree(event) {
        const node: TreeNode = event.node;
        if (node.data.type === 'group') {
            node.children.forEach(child => {
                if (this.event.studentIds.indexOf(child.data.id) !== -1) {
                    child.setIsSelected(true);
                }
            });
        }
    }
    addStudent($event) {
        this.selected.add($event.node.data.id);
    }

    removeStudent($event) {
        this.selected.delete($event.node.data.id);
    }

    onActivate($event) {
        console.log($event);
    }
}
