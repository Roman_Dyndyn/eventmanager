import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Specialty } from 'app/shared/model/specialty.model';
import { SpecialtyService } from './specialty.service';
import { SpecialtyComponent } from './specialty.component';
import { SpecialtyDetailComponent } from './specialty-detail.component';
import { SpecialtyUpdateComponent } from './specialty-update.component';
import { SpecialtyDeletePopupComponent } from './specialty-delete-dialog.component';
import { ISpecialty } from 'app/shared/model/specialty.model';

@Injectable({ providedIn: 'root' })
export class SpecialtyResolve implements Resolve<ISpecialty> {
    constructor(private service: SpecialtyService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((specialty: HttpResponse<Specialty>) => specialty.body));
        }
        return of(new Specialty());
    }
}

export const specialtyRoute: Routes = [
    {
        path: 'specialty',
        component: SpecialtyComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_TEACHER', 'ROLE_ADMIN', 'ROLE_STUDENT'],
            pageTitle: 'eventManagerApp.specialty.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'specialty/:id/view',
        component: SpecialtyDetailComponent,
        resolve: {
            specialty: SpecialtyResolve
        },
        data: {
            authorities: ['ROLE_TEACHER', 'ROLE_ADMIN', 'ROLE_STUDENT'],
            pageTitle: 'eventManagerApp.specialty.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'specialty/new',
        component: SpecialtyUpdateComponent,
        resolve: {
            specialty: SpecialtyResolve
        },
        data: {
            authorities: ['ROLE_TEACHER', 'ROLE_ADMIN'],
            pageTitle: 'eventManagerApp.specialty.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'specialty/:id/edit',
        component: SpecialtyUpdateComponent,
        resolve: {
            specialty: SpecialtyResolve
        },
        data: {
            authorities: ['ROLE_TEACHER', 'ROLE_ADMIN'],
            pageTitle: 'eventManagerApp.specialty.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const specialtyPopupRoute: Routes = [
    {
        path: 'specialty/:id/delete',
        component: SpecialtyDeletePopupComponent,
        resolve: {
            specialty: SpecialtyResolve
        },
        data: {
            authorities: ['ROLE_TEACHER', 'ROLE_ADMIN'],
            pageTitle: 'eventManagerApp.specialty.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
