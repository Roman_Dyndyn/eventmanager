export * from './specialty.service';
export * from './specialty-update.component';
export * from './specialty-delete-dialog.component';
export * from './specialty-detail.component';
export * from './specialty.component';
export * from './specialty.route';
