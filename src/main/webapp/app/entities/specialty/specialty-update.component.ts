import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { ISpecialty } from 'app/shared/model/specialty.model';
import { SpecialtyService } from './specialty.service';
import { IFaculty } from 'app/shared/model/faculty.model';
import { FacultyService } from 'app/entities/faculty';

@Component({
    selector: 'jhi-specialty-update',
    templateUrl: './specialty-update.component.html'
})
export class SpecialtyUpdateComponent implements OnInit {
    specialty: ISpecialty;
    isSaving: boolean;

    faculties: IFaculty[];

    constructor(
        private jhiAlertService: JhiAlertService,
        private specialtyService: SpecialtyService,
        private facultyService: FacultyService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ specialty }) => {
            this.specialty = specialty;
        });
        this.facultyService.query().subscribe(
            (res: HttpResponse<IFaculty[]>) => {
                this.faculties = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.specialty.id !== undefined) {
            this.subscribeToSaveResponse(this.specialtyService.update(this.specialty));
        } else {
            this.subscribeToSaveResponse(this.specialtyService.create(this.specialty));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ISpecialty>>) {
        result.subscribe((res: HttpResponse<ISpecialty>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackFacultyById(index: number, item: IFaculty) {
        return item.id;
    }
}
