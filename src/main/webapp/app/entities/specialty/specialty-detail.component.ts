import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ISpecialty } from 'app/shared/model/specialty.model';

@Component({
    selector: 'jhi-specialty-detail',
    templateUrl: './specialty-detail.component.html'
})
export class SpecialtyDetailComponent implements OnInit {
    specialty: ISpecialty;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ specialty }) => {
            this.specialty = specialty;
        });
    }

    previousState() {
        window.history.back();
    }
}
