import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EventManagerSharedModule } from 'app/shared';
import {
    SpecialtyComponent,
    SpecialtyDetailComponent,
    SpecialtyUpdateComponent,
    SpecialtyDeletePopupComponent,
    SpecialtyDeleteDialogComponent,
    specialtyRoute,
    specialtyPopupRoute
} from './';

const ENTITY_STATES = [...specialtyRoute, ...specialtyPopupRoute];

@NgModule({
    imports: [EventManagerSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        SpecialtyComponent,
        SpecialtyDetailComponent,
        SpecialtyUpdateComponent,
        SpecialtyDeleteDialogComponent,
        SpecialtyDeletePopupComponent
    ],
    entryComponents: [SpecialtyComponent, SpecialtyUpdateComponent, SpecialtyDeleteDialogComponent, SpecialtyDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class EventManagerSpecialtyModule {}
