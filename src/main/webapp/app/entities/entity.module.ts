import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { EventManagerStudentModule } from './student/student.module';
import { EventManagerTeacherModule } from './teacher/teacher.module';
import { EventManagerFacultyModule } from './faculty/faculty.module';
import { EventManagerSpecialtyModule } from './specialty/specialty.module';
import { EventManagerEventModule } from './event/event.module';
import { EventManagerEventStudentModule } from './event-student/event-student.module';
import { EventManagerAdvertisementModule } from './advertisement/advertisement.module';
import { EventManagerGroupModule } from './group/group.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    // prettier-ignore
    imports: [
        EventManagerStudentModule,
        EventManagerTeacherModule,
        EventManagerFacultyModule,
        EventManagerSpecialtyModule,
        EventManagerEventModule,
        EventManagerEventStudentModule,
        EventManagerAdvertisementModule,
        EventManagerGroupModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class EventManagerEntityModule {}
